# -*- coding: cp1251 -*-

from datetime import datetime as dt
from datetime import timedelta as dl
from lib import ImportXLSInMaket
import csv, win32com.client, os, ftplib, codecs, shutil

def DowloadSourceFile(DownloadFile, LocalFile, serv = "ftpuit.moscow.sportmaster.ru", user = "ftp_rms2", pwd = "ftp_rms_ro"):
    ftp = ftplib.FTP(serv)
    ftp.login(user,pwd)    
    dwnRep = open(LocalFile.encode("cp1251"), 'wb')
    ftp.retrbinary("RETR " + DownloadFile, dwnRep.write, 8*1024)
    dwnRep.close()
    print u'Download complite'
    ftp.close()

def decodeFile(inputF,inputCoding, outputCoding):
        outputF = os.path.dirname(inputF) + "codtmp.csv"
        src = codecs.open(inputF, "r", inputCoding)
        new = codecs.open(outputF,"w", outputCoding)
        while True:
                contents = src.read(1048576)
                if not contents:
                        break
                new.write(contents)                
        src.close()
        new.close()        
        os.remove(inputF)
        os.rename(outputF,inputF)
        print u'Decode complite'
                       
date_rep = dt.date(dt.today()) 
#date_rep = dt.date(dt.today()-dl(days=3))
path_to_file = u'//ClusterCO2DFS.gksm.local/DFS/RMSOtchet/����� �� ('+date_rep.strftime('%Y_%m_%d')+'_02_00).csv'
ReportFolder = u'O:\\Users2\\Scm\\�����\\����\\�����������\\'
RepName = u'����� �� ' + date_rep.strftime('%d_%m_%Y')
locFtmp = ReportFolder + RepName + '.csv'
shutil.copyfile(path_to_file, locFtmp)
decodeFile(locFtmp, "utf-16", "cp1251")
TMPFileRep = open(locFtmp, "rb")
csvrep = csv.DictReader(TMPFileRep,delimiter = '\t')
report = []
k = 0 
for rec in csvrep:
    rec['�����, �3'] = rec['�����, �3'].replace(',','.')
    if '���' in rec['������� �������'] or 'MIX_Box' in rec['������� �������'] or '������' in rec['������� �������'] or 'OSTIN' in rec['������� �������'] or '�����' in rec['������� �������']:
        rec['���'] = '���'
    elif '���' in rec['������� �������']:
        rec['���'] = '���'
    elif '��' in rec['������� �������'] or 'BC' in rec['������� �������']:
        rec['���'] = '��'
    elif '����' in rec['������� �������']:
        rec['���'] = '����'
    if rec['���� �������� ����� �� ���'] == '':
        rec['� ���� �� ��'] = rec['�������� �������'] = rec['���������'] = '� ���� �� ��'
        rec['������� �� ��'] = ''
    else:
        rec['� ���� �� ��'] = int((dt.strptime(rec['���� �������� ����� �� ���'], '%d.%m.%Y') - dt.strptime(rec['���� �������� ����� � ���'],'%d.%m.%Y')).days)
        if rec['���� �������� ����� � ���'] == '':
            rec['������� �� ��'] = int((date_rep - dt.date(dt.strptime(rec['���� �������� ����� �� ���'],'%d.%m.%Y'))).days)
            rec['���������'] = '�� ������'
        else:
            rec['������� �� ��'] = int((dt.strptime(rec['���� �������� ����� � ���'],'%d.%m.%Y') - dt.strptime(rec['���� �������� ����� �� ���'],'%d.%m.%Y')).days)
            rec['���������'] = '��������'
        if rec['������� �� ��'] <= 2:
            rec['�������� �������'] = '0-2'
        elif rec['������� �� ��'] <=7:
            rec['�������� �������'] = '3-7'+chr(160)
        elif rec['������� �� ��'] <=14:
            rec['�������� �������'] = '8-14'+chr(160)
        else:
            rec['�������� �������'] = '>14'
    if rec['���� �������� ����� � ���'] == '' or dt.date(dt.strptime(rec['���� �������� ����� � ���'],'%d.%m.%Y')) >= date_rep-dl(days = 7) :
    #if rec['���� �������� ����� � ���'] == '' or dt.strptime(rec['���� �������� ����� � ���'],'%d.%m.%Y') >= dt.strptime("24.06.2013",'%d.%m.%Y'):
        report.append(rec)
TMPFileRep.close()
os.remove(locFtmp) 
rep = list([row['����� ����������� ��'],
            row['���� �������� ����� � ���'],
            row['���� �������� ����� �� ���'],
            row['� ���� �� ��'],
            row['���� �������� ����� � ���'],
            row['������� �� ��'],
            row['�������� �������'],
            row['���������'],
            row['�����-����������'],
            row['�������� ����������'],
            row['��� �� ��� WMS'],
            row['������� �������'],
            row['���-�� (���������, ��.)'],
            row['�����, �3'],
            row['���']] 
           for row in report)
CalcFile = ReportFolder + u'���������.xlsx'
DataCalcList = "����� ��"
ImportXLSInMaket(rep, CalcFile, DataCalcList, 1)
Excel = win32com.client.Dispatch("Excel.Application")
wb = Excel.Workbooks.Open(CalcFile)
Excel.Visible = 1
wb.Sheets(DataCalcList).Name = RepName
wb.SaveAs(ReportFolder + RepName + '.xlsx', FileFormat=51)
wb.RefreshAll()
wb.Save()
Excel.Quit()

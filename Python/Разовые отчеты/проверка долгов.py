# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 13:28:54 2013

@author: IgKuznetsov
"""
import pandas as pd, dateutil
datetext = "18.07.2013"
DolgDate = dateutil.parser.parse(datetext,dayfirst=True)

out = pd.read_csv("out.txt",";", names=['numz','client','ware','qty','tm','dtz','dto','typep','biz','vol'], header=0, parse_dates=[5,6], dayfirst=True)
ideal = pd.read_csv("idelshop.txt")
shop = pd.read_csv("shop.txt","\t")
sklad = pd.read_csv("sklad.csv",";")
tp = pd.read_csv("tp.csv",";") 
a = out.merge(tp, 'left', on=['typep'])
a = a.merge(shop, 'left',on=['client'])
a = a.merge(sklad, 'left', on=['client'])
a = a.merge(ideal,'left', left_on='client', right_on='idealclient')
a.region[a.rrcname.notnull()] = a.rrcname
EnterInfo = a[((a.dto <= DolgDate) & (a.idealclient.isnull())) | ((a.dto < DolgDate) & (a.idealclient.notnull()))]
print EnterInfo[(EnterInfo.tpname == u'МГТ') & (EnterInfo.biz == "O’Stin Россия")]['qty'].sum()
print EnterInfo[(EnterInfo.tpname == u'МГТ') & (EnterInfo.biz == "Спортмастер Россия")]['qty'].sum()
EnterInfo.to_csv("EnterInfo.csv",";")
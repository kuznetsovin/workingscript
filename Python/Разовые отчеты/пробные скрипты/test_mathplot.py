#import pandas
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import matplotlib.cbook as cbook
import matplotlib.dates as dates
import matplotlib.ticker as ticker
import numpy as np
from dateutil import rrule 

#df = pandas.read_csv("D:\\SMNed.csv",sep=";")
datafile = cbook.get_sample_data("D:\\SMNed.csv", asfileobj=False)
r = mlab.csv2rec(datafile, delimiter=";")

x = [i[0] for i in r]
y = [i[1] for i in r]
x_float = [dates.date2num(i[0]) for i in r]


fig = plt.figure()
ax = fig.add_subplot(111)
ax.bar(x,y, width=5, color='red')
#ax.set_ylim(np.min(y), np.max(y))
ax.plot_date(x,y, 'x--')
loc = dates.WeekdayLocator(byweekday=rrule.MO)
ax.xaxis.set_major_locator(loc)
ax.xaxis.set_major_formatter(dates.DateFormatter('%d.%m.%Y'))
fig.autofmt_xdate()
plt.show()
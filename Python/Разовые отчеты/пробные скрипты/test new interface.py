# -*- coding: cp1251 -*-

#from PyQt4.QtGui import QMessageBox
#import guidata
#_app = guidata.qapplication() # not required if a QApplication has already been created
#
#import guidata.dataset.datatypes as dt
#import guidata.dataset.dataitems as di
#
#class Processing(dt.DataSet):
#    """Example"""
#    a = di.FloatItem("Parameter #1", default=2.3)
#    b = di.IntItem("Parameter #2", min=0, max=10, default=5)
#    type = di.ChoiceItem("Processing algorithm",
#                         ("type 1", "type 2", "type 3"))
#    def info(self):
#        q = QMessageBox()
#        q.setText(u"Выполнено")
#        q.exec_()
#
#param = Processing()
#param.edit()
#param.info()

from PyQt4.QtGui import QMessageBox
from formlayout import fedit

datalist = [('Name', 'Paul'),
            ('Age', 30),
            ('Sex', [0, 'Male', 'Female']),
            ('Size', 12.1),
            ('Eyes', 'green'),
            ('Married', True),
            ]

q = QMessageBox()
q.setText(fedit(datalist, title=u"Describe yourself", comment=u"This is just an <b>example</b>.")[0])
q.exec_()
# -*- coding: utf-8 -*-
from pandas import read_csv
from pandas.io import sql
import cx_Oracle
from dateutil import parser as dt_parse
from formlayout import fedit

###############################################################################
#расчет недель опоздания/опережения для целого контейнера
def CalcWeekDelayForContainer(RawDataForOrder):
    #получить список контейнеров с общими процентами опоздания/опережения понедельно
    ContForOrder = RawDataForOrder.pivot_table(['PROCENT'], \
                                                ['CONTAINER', 'WEEK_DELAY'], \
                                                aggfunc='sum').\
                                    reset_index()
    #найти по каждому контейнеру, мах процент отставания\опережения = t
    MaxEDDProcentForCont = ContForOrder.groupby(['CONTAINER'])['PROCENT'].\
                                        max().\
                                        reset_index().\
                                        set_index(['CONTAINER', 'PROCENT'])
    ContForOrder = ContForOrder.set_index(['CONTAINER', 'PROCENT'])
    #получить по всем контейнерам кол-во недель соотвтетсвующих t
    WeekDelayForCont = ContForOrder.join(MaxEDDProcentForCont, how='inner') .\
        reset_index()
    WeekDelayForCont = WeekDelayForCont.drop(['PROCENT'], axis=1)
    #обновить недели внутри каждого контейнера
    RawDataForOrder = RawDataForOrder.merge(WeekDelayForCont, \
                                            'left', ['CONTAINER'], \
                                            suffixes=(['_', ''])).\
                                    drop('WEEK_DELAY_', axis=1)
    return RawDataForOrder
###############################################################################

#объединение информацию по контейнеру
def ConcatContainerInfo(DataSet):
    #получить общее кол-во ед. по контейнеру
    SumCont = DataSet.pivot_table(['QTY', 'STOCK'], \
                                    ['CONTAINER'], \
                                    aggfunc='sum')
    #удалить лишние столцы и дубликаты
    NewDataSet = DataSet.drop(['POTOK', \
                                'WARE', \
                                'QTY', \
                                'STOCK', \
                                'PROCENT', \
                                'FROMSTOR'], axis=1).\
                        drop_duplicates().\
                        set_index('CONTAINER')
    #объединить записи по каждому контейнеру
    for cont in NewDataSet.index:
        tmp = NewDataSet[NewDataSet.index == cont]
        NewDataSet.SHIPMENT[NewDataSet.index == cont] = ";".join(set(tmp.SHIPMENT))
        NewDataSet.ESTDELIVER[NewDataSet.index == cont] = tmp.ESTDELIVER.min()
        NewDataSet.TM[NewDataSet.index == cont] = ";".join(set(tmp.TM))
        NewDataSet.TP[NewDataSet.index == cont] = ";".join(set(tmp.TP))
        NewDataSet.TOSTOR[NewDataSet.index == cont] = tmp.TOSTOR.min()
        NewDataSet.DOC_STATUS[NewDataSet.index == cont] = tmp.DOC_STATUS.min()
    #удалить дубликаты после объединения и присоединям кол-во в контейнере
    DataSet = NewDataSet.drop_duplicates().\
        join(SumCont)
    return DataSet    
###############################################################################

#объединение информацию по shipment
def ConcatShipmentInfo(DataSet):
    #получить общее кол-во ед. по контейнеру
    SumCont = DataSet.pivot_table(['QTY', 'STOCK'], ['SHIPMENT'], aggfunc='sum')
    #удалить лишние столцы и дубликаты
    NewDataSet = DataSet.drop(['POTOK', \
                                'WARE', \
                                'QTY', \
                                'STOCK', \
                                'PROCENT', \
                                'FROMSTOR'], axis=1).\
                        drop_duplicates().\
                        set_index('SHIPMENT')
    #объединить записи по каждому контейнеру
    for cont in NewDataSet.index:
        tmp = NewDataSet[NewDataSet.index == cont]
        NewDataSet.ESTDELIVER[NewDataSet.index == cont] = tmp.ESTDELIVER.min()
        NewDataSet.TM[NewDataSet.index == cont] = ";".join(set(tmp.TM))
        NewDataSet.TP[NewDataSet.index == cont] = ";".join(set(tmp.TP))
        NewDataSet.TOSTOR[NewDataSet.index == cont] = tmp.TOSTOR.min()
        NewDataSet.DOC_STATUS[NewDataSet.index == cont] = tmp.DOC_STATUS.min()
    #удалить дубликаты после объединения и присоединям кол-во в контейнере
    DataSet = NewDataSet.drop_duplicates().\
        join(SumCont)
    return DataSet    
###############################################################################

hub_list = [0, u'Котка', u'Восточный', u'Гамбург', u'Таллин', u'Одесса']
shipto_list = [0, u'Москва, Балашиха', u'Киев']
collection = [0, 'AW11', 'SS11', 'AW12', 'SS12', 'AW13', 'SS13', 'AW14', \
                'SS14', 'AW15', 'SS15', 'AW16', 'SS16']
#создать форму ввода параметров отчета
datalist = [(u'Дата заказа', ''),
            (u'Порт', hub_list),
            (u'Страна', shipto_list),
            (u'Коллекция', collection)]

mainform = fedit(datalist, title=u"Заказ с СПХ")
#try:
#задать параметры отчета
daterep = dt_parse.parse(mainform[0].encode('cp1251'), dayfirst=True)
hub = hub_list[mainform[1]+1].encode('cp1251')
shipto = shipto_list[mainform[2]+1].encode('cp1251')
coll = collection[mainform[3]+1].encode('cp1251')
#подключиться к БД
db = cx_Oracle.Connection("RC", "tx8v64", "LMTESTNW")
param = [hub, shipto, coll]
DateWeekNow = [daterep]
dataset = db.cursor()
#очистить временные таблицы
dataset.execute("TRUNCATE TABLE SPX_SOURCE_DATE")
dataset.execute("TRUNCATE TABLE SPX_FINAL_TABL") 
#заполнить таблицу с данными для расчета                       
dataset.callproc("SPX_GENERATE_SOURCEDATE", param)
db.commit() 
print u'SourceDate заполнен'
#заполнить финальную таблицу для принятия решений     
dataset.callproc("SPX_GENERATED_FINAL_TABLE", DateWeekNow)  
db.commit()   
print u'Final_TAble заполнен'
#загрузить данные
OrderFromHubSrc = sql.read_frame("SELECT * FROM SPX_FINAL_TABL", db)
db.close()
#загрузить POReport
POReport = read_csv("D:/po.txt", 
                    sep="\t", \
                    usecols=['HUB_Shipment', 'HUB_EstDeliver'], \
                    keep_default_na=False, \
                    parse_dates=['HUB_EstDeliver'], 
                    dayfirst=True)
#удалить пустые записи
POReport = POReport.dropna()
#обновить дату EstDeliver по shipment'ам в исходной выгрузке
OrderFromHubSrc = OrderFromHubSrc.merge(POReport, \
                                        'left', \
                                        left_on=['SHIPMENT'], \
                                        right_on=['HUB_Shipment'])
in_rep = OrderFromHubSrc.HUB_EstDeliver.notnull()
OrderFromHubSrc.ESTDELIVER[in_rep] = OrderFromHubSrc.HUB_EstDeliver
OrderFromHubSrc = OrderFromHubSrc.drop(['HUB_Shipment', 'HUB_EstDeliver'], \
                                        axis=1)
#посчитать кол-во недель опаздания/опережения по контейнеру
OrderFromHubSrc = CalcWeekDelayForContainer(OrderFromHubSrc)
#если заказ не по Гаумбургу
if hub_list[mainform[1]+1] != u'Гамбург':
    #объединить информацию по контейнеру
    OrderFromHubSrc = ConcatContainerInfo(OrderFromHubSrc)
else:
    #объединить информацию по shipments
    OrderFromHubSrc = ConcatShipmentInfo(OrderFromHubSrc)
#выгрузить итог
OrderFromHubSrc.to_csv("result.csv", ";")

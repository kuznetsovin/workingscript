# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 09:40:53 2013

@author: IgKuznetsov
"""

import cx_Oracle as oracle
from pandas.io import sql

DB = oracle.Connection("RC", "tx8v64", "LMTESTNW")
WARE_CONDIDATES = sql.read_frame("SELECT * FROM EXPORTVH_CANDIDATES", DB)

PARAM = {
    'weight': 900, \
    'hight': 200, \
    'width': 163, \
    'lenght': 121
    }
    
TEMP = WARE_CONDIDATES[(WARE_CONDIDATES.LENGHT <= PARAM['lenght']) & \
                (WARE_CONDIDATES.WEIGHT <= PARAM['weight']) & \
                (WARE_CONDIDATES.HEIGHT <= PARAM['hight']) & \
                (WARE_CONDIDATES.WIDTH <= PARAM['width'])]

#Плавующие параметры задаются каждый раз
E_TMA_WARE_GROUP = [u'Сервис', u'Одежда']
E_TMA_SUBDIRECTION = [u'Единоборства', \
                    u'Туризм', \
                    u'Тренажеры', \
                    u'Велоспорт', \
                    u'Игры для активного отдыха', \
                    u'Скейтбординг']

TEMP = TEMP.drop([i[0] for i in TEMP.iterrows() \
                        if i[1]['E_TMA_WARE_GROUP'] in E_TMA_WARE_GROUP])
TEMP = TEMP.drop([i[0] for i in TEMP.iterrows() \
                        if i[1]['E_TMA_SUBDIRECTION'] not in E_TMA_SUBDIRECTION])
TEMP = TEMP[TEMP.ED/TEMP.KRAT > 4].set_index('WARE')
TEMP.insert(2, 'PAL_R', TEMP['ED']/TEMP['KRAT'])

sql.execute("truncate table art_igor", DB)
for i in TEMP.index:
    sql.execute("insert into art_igor values ('%s')" % i, DB)
IO_TOV = sql.read_frame("SELECT * FROM DVIG_TOVAR3MON", DB)
PIVOT_IO_TOV = IO_TOV.pivot_table(['QTY'], ['IDPR'], ['TYPE', 'MES'], 'sum')
PIVOT_IO_TOV.to_csv('o.csv')

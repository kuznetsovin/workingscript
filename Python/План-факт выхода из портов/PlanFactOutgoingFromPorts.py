# -*- coding: cp1251 -*-

import pandas, cx_Oracle
from pandas.io import sql
#��������� ����� �� ���������
SHIPMENT_REPORT = 'd:\\shipmentYUVA.txt'
PORT = ['NKM/NKM FI CY (Kotka)',
        'LOGO/LOGO EE CY TLL (Tallinn)',
        'VIFS/VIFS RU CY (Vostochny)',
        'SM/_RF Moscow Custom WHS',
        'ITLC/ITLC DE HUB (Hamburg)',
        'B2B/B2B DE HUB (Hamburg)',
        'TRXP/TRXP RU CY (Novorossiysk)']

sh_rep = pandas.read_csv(SHIPMENT_REPORT, sep='\t')
sh_rep.ActualDate = pandas.to_datetime(sh_rep.ActualDate.str[:10], \
                                    dayfirst=True, coerce=True)
#���������� ������������� ��������
zabaykal = sh_rep[(sh_rep.Status==90) \
                & (sh_rep.FROM.str.contains('Office')) \
                & (sh_rep.Destination == 'SM/_RF FDC (157)') \
                & (sh_rep['Carrier location'].notnull()) \
                & (sh_rep['Carrier location'].notnull()) \
                & (sh_rep['Carrier location'] != 'SM Local Carrier / SMCRR ����� �������') \
                & (sh_rep['Carrier location'] != 'SM Local Carrier / SMCRR ����� ����������') \
                & (sh_rep['Carrier location'] != 'SM Local Carrier / SMCRR ����� �����������') \
                & (sh_rep['Carrier location'] != 'SM Local Carrier / SMCRR ����� �����������') \
                & (sh_rep['Carrier location'] != 'SPORTMASTER Manual / SMCRR ����� ������')]
#�������� ������������� ������ ���������
sh_rep = sh_rep[sh_rep.Destination.isin(PORT)]
sh_rep = sh_rep.append(zabaykal)
sh_rep.EventName[sh_rep.Code == 72070] = 'OUOB'
sh_rep.EventName[sh_rep.Code == 2090] = 'Shipped'
#��������� ������� �� ����� OUOB � Shipped
sum_tabl = sh_rep.pivot_table(['ActualDate'], ['Shipment'], ['EventName'], 'min')
sum_tabl.index = sum_tabl.index.astype(str)
print u'������� ���������'
db = cx_Oracle.Connection("RC/tx8v64@LMTESTNW")
#�������� ������ ����������� ���������
POD = sql.read_frame("SELECT DISTINCT * FROM pf_outgoing_date", db, \
                    index_col='SHIPMENT')
#POD.SHIPPEDDATE = pandas.to_datetime(POD.SHIPPEDDATE)
#�������� ������ ��������� � ����������� �������� OUOB
ouob_dt_upd = sum_tabl['ActualDate', 'OUOB'][sum_tabl['ActualDate', 'OUOB'].\
                    notnull()]
shipment_for_upd = tuple(POD[POD.WORKSTATUS == 'Shipped'].\
                        join(ouob_dt_upd, how='inner').index)
ResulTableDB = db.cursor()
#������� ��� �������� ��� �����������
if len(shipment_for_upd) > 0:
    print shipment_for_upd
    ResulTableDB.execute("DELETE FROM pf_outgoing_date WHERE shipment in %s" % str(shipment_for_upd))
    ResulTableDB.execute("DELETE FROM pf_main WHERE shipment IN %s" % str(shipment_for_upd))
    db.commit()
#�������� ������ ���������, ������� ����� ��������
new_pod = sum_tabl.join(POD)
new_pod['ActualDate', 'OUOB'] = pandas.to_datetime(new_pod['ActualDate', 'OUOB'], coerce=True)
new_pod['ActualDate', 'Shipped'] = pandas.to_datetime(new_pod['ActualDate', 'Shipped'], coerce=True)
new_pod.SHIPPEDDATE[new_pod['ActualDate','OUOB'].notnull()] = new_pod['ActualDate','OUOB']
new_pod.SHIPPEDDATE[new_pod['ActualDate','OUOB'].isnull()] = new_pod['ActualDate','Shipped']
shipment_for_ins = tuple(new_pod.index[new_pod.WORKSTATUS.isnull()])
new_pod.WORKSTATUS[new_pod['ActualDate','OUOB'].notnull()] = 'OUOB'
new_pod.WORKSTATUS[new_pod['ActualDate','OUOB'].isnull()] = 'Shipped'
LoadToDB = new_pod[['SHIPPEDDATE','WORKSTATUS']][new_pod.index.isin(shipment_for_ins + shipment_for_upd)]
for rec in LoadToDB.itertuples():
    ResulTableDB.execute("INSERT INTO pf_outgoing_date VALUES (:1,:2,:3)", rec)
db.commit()
print u'�������� ����������'
ResulTableDB.callproc("PF_UPDATE_MAIN_TABLE")
print u'��������� ������� PF_Main'
db.commit()
db.close()

# -*- coding: utf-8 -*-
"""
Created on Fri Jul 26 14:09:06 2013

@author: IgKuznetsov
"""
from pandas import read_csv, DataFrame

po = read_csv('D:/po.txt', '\t')
containers = read_csv('D:/containers.txt', index_col=[0])

tohub = po[['Container To HUB','Accepted Qty','EndDeliver']][po['Container From HUB'].isnull()].dropna().set_index(['Container To HUB'])
fromhub = po[['Container From HUB', 'Accepted Qty', 'EndDeliver']].\
			dropna().\
			set_index(['Container From HUB'])
pocont = tohub.append(fromhub)
c = pocont.groupby([pocont.index, 'EndDeliver']).\
    sum().\
    reset_index(1)
d = c.groupby(c.index).max()['Accepted Qty'].astype(float).astype(int)

t = c.merge(DataFrame(d), left_index=True, right_index=True)
t = t.EndDeliver[t['Accepted Qty_x'] == t['Accepted Qty_y']]
containers.join(t).to_csv('prior.csv', ';')

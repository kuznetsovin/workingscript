# -*- coding: cp1251 -*- 

import wx, DelimiterFile as df

class MForm (wx.Frame):
    __fInputCSV = ""
    def __init__(self, title):
        wx.Frame.__init__(self, None, title=title, size=(150, 130))
        self.SetBackgroundColour((255, 255, 255))
        font = wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD)
        wx.StaticText(self, label="�������� ����", pos=(10, 10)).SetFont(font)
        self.FilePath = wx.TextCtrl(self, pos=(10, 30))
        btnOpen = wx.Button(self, label="...", pos=(110, 30), size=(20, 20))
        btnOpen.SetFont(font)
        btnOpen.Bind(wx.EVT_BUTTON, self.onOpenFile)
        btnOk = wx.Button(self, label="���������", pos=(10, 60), size=(80, -1))
        btnOk.SetFont(font) 
        btnOk.Bind(wx.EVT_BUTTON, self.onClickOK) 
        self.Show(True)
    def onOpenFile(self, event):
        dlg = wx.FileDialog(self, message="�������� ����", \
                            style=wx.OPEN | wx.CHANGE_DIR)
        if dlg.ShowModal() == wx.ID_OK:
            self.FilePath.SetValue(dlg.GetPath())
        dlg.Destroy()
    def onClickOK(self, event):
        try:
            df.DelimiterCSV(self.FilePath.GetValue())
            wx.MessageBox("���������� ���������", "���������", wx.OK)
        except IndexError:
            wx.MessageBox("� ����� ���� ������ ��� �������� ������.", "Error", wx.OK)
        

app = wx.App(False)
frame = MForm('�������� �����')
app.MainLoop()

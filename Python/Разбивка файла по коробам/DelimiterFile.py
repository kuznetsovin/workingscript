# -*- coding: cp1251 -*-
import csv, os, shutil

def DelimiterCSV(file_path):
    # ������ ������ csv
    f = open(file_path)
    f_input = csv.reader(f, delimiter="\t")
    arrCsv = list(rec for rec in f_input)
    f.close() 
    full_path_file = os.path.join(os.path.dirname(file_path), "result")
    # �������� ���� ������� (��)
    lstTypeKor = list(set(rec[2] for rec in arrCsv))    
    if os.path.exists(full_path_file):
        shutil.rmtree(full_path_file)
    os.mkdir(full_path_file)

    # ��������� �� ������� �������� �� ��
    for rec in lstTypeKor:    
        # ����� � ���� ��[i].txt    
        f = open(os.path.join(full_path_file, "%s.txt" % rec), "wb")
        fKorCSV = csv.DictWriter(f, fieldnames=['ART', 'QTY'], delimiter="\t")
        fKorCSV.writeheader()
        # ���� ��� �� �� ������������ ���� �� csv
        fKorCSV.writerows([{'ART':row[0], 'QTY':row[1]} for row in arrCsv \
                                                            if row[2] == rec])
        f.close()

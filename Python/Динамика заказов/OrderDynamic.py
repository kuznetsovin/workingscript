# -*- coding: utf-8 -*-
"""
Created on Tue Aug 27 17:47:10 2013

@author: IgKuznetsov
"""
import EasyDialogs, cx_Oracle, traceback, sys
from pandas import read_csv
from formlayout import fedit
from datetime import datetime as dt
from datetime import timedelta as dl

datalist = [(u'Заказы на дату', dt.date(dt.today())-dl(days=1))]
rep_date =  fedit(datalist, title=u'Динамика долгов')

try:    
    DB = cx_Oracle.Connection('RC', 'tx8v64', 'LMTESTNW')
    rc = DB.cursor()
    bar = EasyDialogs.ProgressBar(maxval=4)
    rc.execute("""SELECT distinct client FROM order_dynamic od 
                MINUS 
                SELECT clientid FROM client_dict cd""")
    bar.inc()
    NoShop = [str(i[0]) for i in rc.fetchall()]
    title = [
        u'Нет магазинов в справочнике: %s'.encode('cp1251'),
        u'Выберите файл справочника магазинов'.encode('cp1251'),
        u'Данные обновлены.'.encode('cp1251')
        ]
    if NoShop is not None:
        EasyDialogs.Message(title[0] % ','.join(NoShop))
        filename = EasyDialogs.AskFileForOpen(title[1])
        shops = read_csv(filename, ';', index_col=0)
        ShopForLoad = shops[['Идеальный магазин', \
                            'Название', \
                            'Регион', \
                            'Торговая сеть']]
        ShopForLoad['Идеальный магазин'] = ShopForLoad['Идеальный магазин'].\
                                                str.replace('Нет', '0')
        ShopForLoad['Идеальный магазин'] = ShopForLoad['Идеальный магазин'].\
                                                str.replace('Да', '1')
        bar.inc()
        rc.execute("""DELETE FROM client_dict 
                        WHERE div <> 'Региональный РЦ'""".decode('utf-8'))
        ShopForLoad['Название'] = ShopForLoad['Название'].str.decode('utf-8')
        ShopForLoad['Название'] = ShopForLoad['Название'].str.replace("'", "`")
        ShopForLoad['Регион'] = ShopForLoad['Регион'].str.decode('utf-8')
        ShopForLoad['Торговая сеть'] = ShopForLoad['Торговая сеть'].\
                                            str.decode('utf-8')
        for clt in ShopForLoad.itertuples():
            ins_client = list(clt)
            ins_client[0] = str(ins_client[0])
            val = "','".join(ins_client)
            rc.execute("""INSERT INTO client_dict 
                                VALUES ('%s', NULL, NULL, NULL)""" % val)
        DB.commit()
        bar.inc()
    bar.inc()
    rc.callproc("load_order_dynamic", rep_date)
    DB.commit() 
    bar.__del__()
    EasyDialogs.Message(title[2])  
except Exception:
    bar.__del__()
    print ''.join(traceback.format_exception(*sys.exc_info()))
    EasyDialogs.Message(traceback.format_exception(*sys.exc_info())[2])
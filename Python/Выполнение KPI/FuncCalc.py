# -*- coding: utf-8 -*-
"""
Created on Thu Aug 22 09:26:51 2013

@author: IgKuznetsov
"""

from pandas import read_csv, ExcelWriter, to_datetime
from pandas.io import sql
import cx_Oracle

def CalculetedKPI(div, coll, xls=False):
    div = {
        'SM':"tm not IN ('O`Stin','FunDay')", 
        'OST':"tm in ('O`Stin','FunDay')"
        } 
        
    db_connection = cx_Oracle.Connection("RC", "tx8v64", "LMTESTNW")
    #Выгризть из EEM_SPECIFICATION_TABLE все PO по нужной коллекции
    query = u"""SELECT num_po po,
                    shipment,
                    fromstor,
                    tostor,
                    hub1,
                    endshipdate esd,
                    enddeliverdate edd,
                    sum(qty) qty 
               FROM eem_specification_table est 
               WHERE shipto = 'Москва, Балашиха' AND collection = '%s' AND %s
               GROUP BY num_po,
                   shipment,
                   fromstor,
                   tostor,
                   hub1,
                   endshipdate,
                   enddeliverdate""" % (coll, div[div])
    eem_sp_tabl = sql.read_frame(query, db_connection)
    #убирать шипменты до CFS
    eem_sp_tabl = eem_sp_tabl[~eem_sp_tabl.TOSTOR.str.contains('(EEM)')]
    #разделить выгрузку на до/после хаба
    eem_sp_tabl.insert(0, 'PATH', 'to_hub')
    eem_sp_tabl.PATH[eem_sp_tabl.TOSTOR == u'Москва, Балашиха'] = 'from_hub'
    #загрузить отчет по PO из EEMRepotrs
    po_report = read_csv('po.txt','\t', \
                usecols=['PO', \
                        'Collection', \
                        'OwnerDivision', \
                        'Status', \
                        'Hub1', \
                        'StipTo', \
                        'OrderedQty', \
                        'EndShip', \
                        'EndDeliver', \
                        'HUB_Shipment', \
                        'ShipTo_Shipment', \
                        'SchStatus'], \
                keep_default_na=False, \
                parse_dates=['EndShip', 'EndDeliver'], \
                dayfirst=True)
    if div == 'OST':
        po_report = po_report[(po_report.StipTo == '_RF FDC (157)') & \
                            (po_report.SchStatus == 'Activated') & \
                            (po_report.Status != 'Canceled') & \
                            (po_report.OwnerDivision == 'OST') & \
                            (po_report.Collection == coll)]
    else:
        po_report = po_report[(po_report.StipTo == '_RF FDC (157)') & \
                            (po_report.SchStatus == 'Activated') & \
                            (po_report.Status != 'Canceled') & \
                            (po_report.OwnerDivision != 'OST') & \
                            (po_report.Collection == coll)]
        po_report.drop_duplicates(inplace=True)
    po_report = po_report.drop(['Collection', 'OwnerDivision', 'SchStatus'], \
                                axis=1)
    #Загрузить архив отчетов по Shipment из EEMRepotrs
    shipment_event_history = read_csv('shipment.txt', \
                                    '\t', \
                                    dtype={'Code':object, 'Shipment':object}, \
                                    usecols=['Code', \
                                            'ActualDate', \
                                            'Shipment', \
                                            'FROM', \
                                            'Destination', \
                                            'Carrier location'])
    #выбираем из отчета события Shipped, Arrived, OUOB
    events = ['2090', '2092', '72070']
    shipment = shipment_event_history[(shipment_event_history.Code.isin(events)) & \
                                    (shipment_event_history.ActualDate.notnull())]
    shipment.ActualDate = to_datetime(shipment.ActualDate[:10], dayfirst=True)
    shipment.drop_duplicates(inplace=True)
    #определить забайкальские шипменты
    zabaykal = shipment.Shipment[(shipment.FROM.str.contains('Office')) & \
        (shipment.Destination == 'SM/_RF FDC (157)') & \
        (shipment['Carrier location'].notnull()) & \
        (shipment['Carrier location'] != u'SM Local Carrier / SMCRR склад Ташкент') & \
        (shipment['Carrier location'] != u'SM Local Carrier / SMCRR склад Поставщика') & \
        (shipment['Carrier location'] != u'SM Local Carrier / SMCRR склад Райхерсберг') & \
        (shipment['Carrier location'] != u'SPORTMASTER Manual / SMCRR склад Верона')]
    #построить сводную по событиям шипментов
    event_shipment_summary = shipment.pivot_table(['ActualDate'], \
                                                ['Shipment'], \
                                                ['Code'], \
                                                'min')
    #определить PO, которых нет в вызруке из EEM_SPECIFICATION_TABLE
    no_po = po_report.merge(eem_sp_tabl, 'left', 'PO')
    no_po = no_po[no_po.PATH.isnull()]
    #определить недостающие шипменты
    no_sh = [no_po.HUB_Shipment[no_po.HUB_Shipment != ''].drop_duplicates()] + \
        [no_po.ShipTo_Shipment[no_po.ShipTo_Shipment != ''].drop_duplicates()]
    #обновить информацию по недостающих шипментам из EEM_SPECIFICATION
    query = """SELECT num_po po,
                    shipment,
                    fromstor,
                    tostor,
                    hub1,
                    endshipdate esd,
                    enddeliverdate edd,
                    sum(qty) qty
            FROM eem_specification est 
            WHERE shipment IN ('%s') 
            GROUP BY num_po,shipment,
                    fromstor,
                    tostor,
                    hub1,
                    endshipdate,
                    enddeliverdate""" % ("','".join(no_sh))
    no_load_eemst = sql.read_frame(query, db_connection)

    no_load_eemst.insert(0, 'PATH', 'to_hub')
    no_load_eemst.PATH[no_load_eemst.TOSTOR == u'Москва, Балашиха'] = 'from_hub'
    #Добавить PO, обновленные по EEM_SPECIFICATION
    final_eem = eem_sp_tabl.append(no_load_eemst)
    #Проверить PO по которым не было еще движения
    no_po = po_report.merge(final_eem, 'left', 'PO')
    add_po = no_po[no_po.PATH.isnull()]
    #Добавить их в итоговую таблицу и до хаба и до фрц
    add_po.EDD = add_po.EndDeliver
    add_po.ESD = add_po.EndShip
    add_po.QTY = add_po.OrderedQty
    add_po.HUB1 = add_po.Hub1
    add_po = add_po.drop(['Status', \
                            'Hub1', \
                            'StipTo', \
                            'OrderedQty', \
                            'EndShip', \
                            'EndDeliver', \
                            'HUB_Shipment', \
                            'ShipTo_Shipment'], \
                        axis=1)
    add_po.PATH = 'to_hub'
    final_eem = final_eem.append(add_po)
    add_po.PATH = 'from_hub'
    final_eem = final_eem.append(add_po)
    #узнать забайкальские шипменты до ФРЦ
    zabaykal_to_hub = final_eem[final_eem.SHIPMENT.isin(zabaykal)]
    zabaykal_to_hub.PATH = 'to_hub'
    #добавить забайкальские шипменты до хаба
    final_eem = final_eem.append(zabaykal_to_hub)
    #узнать po, которые есть до хаба и нет до фрц
    add_from_hub = final_eem[ \
        (~final_eem.PO.isin(final_eem.PO[final_eem.PATH == 'from_hub'])) & \
        (final_eem.PO.isin(final_eem.PO[final_eem.PATH == 'to_hub'])) \
        ]
    add_from_hub.shipment = ''
    add_from_hub.PATH = 'from_hub'
    add_from_hub.FROMSTOR = add_from_hub.TOSTOR
    add_from_hub.TOSTOR = u'Москва, Балашиха' 
    final_eem = final_eem.append(add_from_hub)
    #выгрузить итог
    itog = final_eem.set_index('SHIPMENT').join(event_shipment_summary)
    itog.HUB1 = itog.HUB1.str.decode('cp1251')
    itog.TOSTOR = itog.TOSTOR.str.decode('cp1251')
    itog.FROMSTOR = itog.FROMSTOR.str.decode('cp1251')
    itog.rename(columns={('ActualDate', '2090'):'SHIPPED', \
                        ('ActualDate', '2092'):'ARRIVED', \
                        ('ActualDate', '72070'):'OUOB'}, \
                inplace=True)
    #Копируем внутренних поставщиков в выход
    copy_vp = itog[itog.HUB1.isnull()]
    copy_vp.PATH = 'to_hub'
    itog = itog.append(copy_vp)
    if xls:
        writer = ExcelWriter('result%s%s.xlsx' % (div, coll)) 
        itog[itog.PATH == 'to_hub'].to_excel(writer, u'До Hub', \
                                            index_label='SHIPMENT')
        itog[itog.PATH == 'from_hub'].to_excel(writer, u'До ФРЦ', \
                                            index_label='SHIPMENT')
        writer.save()
    else:
        itog.to_csv('result/result%s%s.csv' % (div, coll), \
                    index_label='SHIPMENT')

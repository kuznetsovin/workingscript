# -*- coding: utf-8 -*-
"""
Created on Thu Aug 22 09:26:51 2013

@author: IgKuznetsov
"""

from pandas import read_csv
from pandas.io import sql
from datetime import date
from FuncCalc import CalculetedKPI
import cx_Oracle

FILTER_DT = date(2013, 9, 1)
DIV = ['SM', 'OST']
COLL = ['AW14', 'SS14']
DB = cx_Oracle.Connection("RC", "tx8v64", "LMTESTNW")

BUF = []
for i in DIV:
    for j in COLL:        
        CalculetedKPI(i, j)
        tmp = read_csv('result/result%s%s.csv' % (i, j), \
        				parse_dates=['EDD', 'ESD', 'SHIPPED', 'ARRIVED', 'OUOB'])
        tmp.insert(len(tmp.columns), 'DIV', i)
        tmp.insert(len(tmp.columns), 'COLL', j)
        BUF.append(tmp)
        print "Load division: %s, collection: %s" % (i, j)

SUMMARY = BUF[0]

for i in range(1, len(BUF)):
    SUMMARY = SUMMARY.append(BUF[i])

FINAL = SUMMARY[(SUMMARY.EDD >= FILTER_DT) | (SUMMARY.ESD >= FILTER_DT)]

#обрабатываем дивизион
FunDay = sql.read_frame("""SELECT DISTINCT po 
						FROM pf_main pm 
						WHERE type_pf = 'План' AND tm = 'FunDay'""", DB)
FINAL.DIV[FINAL.PO.isin(FunDay.PO)] = 'FunDay'
FINAL.DIV[(FINAL.DIV == 'SM') & (FINAL.HUB1.isnull())] = u'SM (ВП)'
FINAL.DIV[(FINAL.DIV == 'SM') & (FINAL.HUB1.notnull())] = u'SM (ЗП)'

        
FINAL.to_csv(u'сводная.csv', ';', index=False)

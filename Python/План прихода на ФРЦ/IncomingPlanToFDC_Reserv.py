# -*- coding: utf-8 -*-
"""
Резервная копия плана прихода на случай недоступности базы
@author: IgKuznetsov
"""
from pandas import read_csv
from pandas.io import sql
import cx_Oracle
#загружаем PO и ShipmentReport и шипменты со статусом pending
POReport = read_csv("d:/po.txt", "\t", \
                    dtype={'HUB_Shipment':object, 'ShipTo_Shipment':object}, \
                    usecols=['StipTo', \
                            'HUB_Shipment', \
                            'HUB_EstDeliver', \
                            'ShipTo_Shipment', \
                            'ShipTo_EstDeliver'], \
                    keep_default_na=False, \
                    parse_dates=['HUB_EstDeliver', 'ShipTo_EstDeliver'], \
                    dayfirst=True)
Shipments = read_csv("d:/sh.txt", "\t", \
                    dtype={'Shipment':object}, \
                    usecols=['Code', \
                    'ActualDate', \
                    'Shipment', \
                    'Status', \
                    'FROM', \
                    'Destination', \
                    'Carrier location', \
                    'Container'], \
                    keep_default_na=False, parse_dates=['ActualDate'], \
                    dayfirst=True)

POReport = POReport[POReport.StipTo == '_RF FDC (157)']
#получаем плановые даты по шипментам и текущие шипменты
a = POReport
POReport.HUB_EstDeliver[a.ShipTo_Shipment != ''] = a.ShipTo_EstDeliver[a.ShipTo_Shipment != '']
POReport.HUB_Shipment[a.ShipTo_Shipment != ''] = a.ShipTo_Shipment[a.ShipTo_Shipment != '']
POReport.insert(0, 'Step', '')
POReport.Step[a.HUB_Shipment != a.ShipTo_Shipment] = u'До СПХ'
POReport.Step[a.HUB_Shipment == a.ShipTo_Shipment] = u'До ФРЦ'
POReport = POReport.drop(['StipTo', 'ShipTo_Shipment', 'ShipTo_EstDeliver'], \
                        axis=1)
POReport.rename(columns={'HUB_Shipment':'Shipment', 
                         'HUB_EstDeliver':'EstDeliver'}, 
                inplace=True)
POReport = POReport[POReport.EstDeliver.notnull()]
POReport.drop_duplicates(inplace=True)
POReport.set_index('Shipment', inplace=True)
sh = Shipments[['Shipment', 'Status']].drop_duplicates()
sh.set_index('Shipment', inplace=True)
EstForSh = POReport.join(sh)
EstForSh = EstForSh[~((EstForSh.Step == u'До ФРЦ') & (EstForSh.Status > 90))]
#Добавлем статус шипментов и доп инфу по нему
ShipmentReport = Shipments[['Shipment', 'FROM', 'Destination', 'Container']].\
                drop_duplicates()
ShipmentReport.set_index('Shipment', inplace=True)
eem = ShipmentReport.join(EstForSh, how='inner')
eem.Step[(eem.Step == u'До СПХ') & (eem.Status > 90)] = u'На СПХ'
db = cx_Oracle.Connection("RC", "tx8v64", "LMTESTNW")
rc = db.cursor()
rc.execute("TRUNCATE TABLE shipment_temp")
for shipment in eem.index:
    rc.execute("INSERT INTO shipment_temp VALUES ('%s')" % (shipment))    
db.commit()
TmForShipment = sql.read_frame("SELECT * FROM RC.TM_FOR_SHIPMENT", db, \
                            index_col='SHIPMENT')
eem = eem.join(TmForShipment)
HUBS = ['B2B/B2B DE HUB (Hamburg)',
        'NKM/NKM FI CY (Kotka)',
        'ITLC/ITLC DE HUB (Hamburg)',
        'VIFS/VIFS RU CY (Vostochny)',
        'LOGO/LOGO EE CY TLL (Tallinn)',
        'TRXP/TRXP RU CY (Novorossiysk)', 
        u'Забайкалье']
#Добавлям к контейнерам контейнеры забайкалье
Zabaykal = Shipments.Shipment[(Shipments.Code==2090) 
                                & (Shipments.Status==90) 
                                & (Shipments.FROM.str.contains('Office'))                
                                & (Shipments.Destination == 'SM/_RF FDC (157)')
                                & (Shipments['Carrier location'].notnull())
                                & (Shipments['Carrier location'] != 'SM Local Carrier / SMCRR склад Ташкент')
                                & (Shipments['Carrier location'] != 'SM Local Carrier / SMCRR склад Поставщика')
                                & (Shipments['Carrier location'] != 'SM Local Carrier / SMCRR склад Фельдкирхен')
                                & (Shipments['Carrier location'] != 'SM Local Carrier / SMCRR склад Райхерсберг')
                                & (Shipments['Carrier location'] != 'SPORTMASTER Manual / SMCRR склад Верона')]
eem.Destination[eem.index.isin(Zabaykal)] = u'Забайкалье'
#группируем до контейнера
eem.Destination[eem.Destination == 'SM/_RF FDC (157)'] = eem.FROM[eem.Destination == 'SM/_RF FDC (157)']
eem = eem[eem.Destination.isin(HUBS)]
eem.Container[(-eem.Container.str.contains('[a-zA-z]{4}\d{7}') & -eem.Container.str.contains('/'))] = eem.index
eem = eem.reset_index().drop(['Shipment', 'FROM', 'Status'], axis=1)
#убираем дубликаты
for cont in eem.Container[eem.TM.notnull()].drop_duplicates():    
    NumCont = eem[eem.Container == cont]
    eem.TM[eem.Container == cont] = ';'.join(set((";".join(NumCont.TM.dropna())).split(';'))) 
    eem.TYPENAME[eem.Container == cont] = ';'.join(set((";".join(NumCont.TYPENAME.dropna())).split(';')))
    eem.Step[eem.Container == cont] = NumCont.Step.min()
    eem.EstDeliver[eem.Container == cont] = NumCont.EstDeliver.min()
    eem.KOL[eem.Container == cont] = NumCont.KOL.sum()
eem.drop_duplicates(inplace=True)
#Добавлям к контейнерам контейнеры со статусом pending
pending = read_csv('d:/PendingShipment.csv', \
                usecols=['From', 'Container#', 'Est deliver'], \
                parse_dates=['Est deliver'], encoding='utf-16').\
        rename(columns={'Container#':'Container'})
pend_cont = pending.Container[pending.From == 'VIFS/VIFS RU CY (Vostochny)']
eem.Step[eem.Container.isin(pend_cont)] = u'До ФРЦ'
pendingDate = pending.groupby('Container').min().reset_index()
eem = eem.merge(pendingDate, 'left', on='Container')
eem.EstDeliver[eem['Est deliver'].notnull()] = eem['Est deliver']
eem.Destination[eem.From.notnull()] = eem.From
eem = eem.drop(['From', 'Est deliver'], axis=1)
#добавляем Logistic (файл для вход обрабатывается вручную)
logistic = read_csv("parts.csv",";", keep_default_na=False, \
                    parse_dates=['EstDeliver'], dayfirst=True)
report = eem.append(logistic)
report.to_csv("Incoming_plan.csv")

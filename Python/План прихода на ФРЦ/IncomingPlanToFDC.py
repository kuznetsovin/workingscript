# coding: cp1251
import cx_Oracle
from pandas import read_csv
from pandas.io import sql
from datetime import date

db = cx_Oracle.Connection("RC/tx8v64@LMTESTNW")
#��������� ������� ����������� � ����

COLS = ['Shipment to HUB', 
        'Est Deliver To HUB',
        'Shipment From HUB',
        'Est Deliver From HUB']

report = sql.read_frame("SELECT * FROM RC.PLAN_INCOMING", db, ['SHIPMENT'])
#��������� ����� �� po
POReport = read_csv("d:/po.txt", "\t", \
                dtype={COLS[0]:object, COLS[2]:object}, \
                usecols=COLS, \
                keep_default_na=False, \
                parse_dates=[COLS[1], COLS[3]], dayfirst=True)

POReport.rename(columns={COLS[0]:'HUB_Shipment', \
                        COLS[1]:'HUB_EstDeliver', \
                        COLS[2]:'ShipTo_Shipment', \
                        COLS[3]:'ShipTo_EstDeliver'}, inplace=True)


##�������� �� ���� estdelive �� hub � ������������� ��� � ����
ToHUBEstDate = POReport[['HUB_Shipment', 'HUB_EstDeliver']].\
                dropna().\
                drop_duplicates()
ToHUBEstDate.set_index('HUB_Shipment', inplace=True)
##�������� �� ���� estdelive �� fdc � ������������� ��� � ����
FromHUBEstDate = POReport[['ShipTo_Shipment', 'ShipTo_EstDeliver']].\
                dropna().\
                drop_duplicates()
FromHUBEstDate.set_index('ShipTo_Shipment', inplace=True)
#�������� ����������� ���� EstDeliver
report = report.join(ToHUBEstDate).\
        join(FromHUBEstDate)

report.ESTDELIVERDATETIME[report.HUB_EstDeliver.notnull()] = report.HUB_EstDeliver
report.ESTDELIVERDATETIME[report.ShipTo_EstDeliver.notnull()] = report.ShipTo_EstDeliver
#�������� ��������� �� �������, ��� ��� ������
report.CONTAINER[report.CONTAINER.isnull()] = report.index
report.CONTAINER[(-report.CONTAINER.str.contains('[a-zA-z]{4}\d{7}') & -report.CONTAINER.str.contains('/'))] = report.index
#������� �������� ������� � �������� �����
report = report.reset_index().\
                drop(['index', 'HUB_EstDeliver', 'ShipTo_EstDeliver'], axis=1).\
                drop_duplicates()
#��������� ����� �� shipment'��
ShipmentEvents = read_csv("d:/sh.txt", "\t", \
                        usecols=[0, 12, 13, 15, 16, 23, 24], \
                        index_col=[1])
#������� ������������� ����������
ShipmentEvents = ShipmentEvents[(ShipmentEvents.Code==2090) \
            & (ShipmentEvents.Status==90) \
            & (ShipmentEvents.FROM.str.contains('Office')) \
            & (ShipmentEvents.Destination == 'SM/_RF FDC (157)') \
            & (ShipmentEvents['Carrier location'].notnull()) \
            & (ShipmentEvents['Carrier location'] != 'SM Local Carrier / SMCRR ����� �������') \
            & (ShipmentEvents['Carrier location'] != 'SM Local Carrier / SMCRR ����� ����������') \
            & (ShipmentEvents['Carrier location'] != 'SM Local Carrier / SMCRR ����� �����������') \
            & (ShipmentEvents['Carrier location'] != 'SM Local Carrier / SMCRR ����� �����������') \
            & (ShipmentEvents['Carrier location'] != 'SPORTMASTER Manual / SMCRR ����� ������')]
#���������� �� �-��������� �� � ���                             
query = """SELECT DISTINCT shipment, est.tm, tpl.TYPENAME
            FROM eem_specification_table est 
                INNER JOIN catalog_ware cw ON est.ware = cw.ware
                INNER JOIN type_preparation_lm7 tpl ON cw.type_preparation = tpl.typeid
            WHERE shipment IN ('%s')""" % ("','".join(ShipmentEvents.index))
TmAndType = sql.read_frame(query, db, ['SHIPMENT'])
Zabaykal = ShipmentEvents.join(TmAndType)
#���������� �� �-����������� estdeliver
Zabaykal = Zabaykal.join(FromHUBEstDate)
#���������� ShipmentEvents ��� ���������� � ������
Zabaykal.Container[(-Zabaykal.Container.str.contains('[a-zA-z]{4}\d{7}'))] = Zabaykal.index
Zabaykal = Zabaykal.reset_index(drop=True)
Zabaykal = Zabaykal.drop(['Code', \
                            'Status', \
                            'FROM', \
                            'Destination', \
                            'Carrier location'], \
                        axis=1)
Zabaykal.insert(0, 'STEPCHAIN', '�� ���')
Zabaykal.insert(1, 'FROMSTOR', '����������')
Zabaykal = Zabaykal.drop_duplicates()
Zabaykal = Zabaykal.rename(columns={'Container':'CONTAINER',
                                    'ShipTo_EstDeliver':'ESTDELIVERDATETIME'})
#������ ������������� ���������� � �����
report = report.append(Zabaykal)
#���������� ���������� �� ����������� �� ������
for cont in report.CONTAINER[report.CONTAINER.duplicated()]:
    NumCont = report[report.CONTAINER == cont]
    report.TM[report.CONTAINER == cont] = ";".join(set(NumCont.TM))
    report.TYPENAME[report.CONTAINER == cont] = ";".join(set(NumCont.TYPENAME.dropna()))
    report.ESTDELIVERDATETIME[report.CONTAINER == cont] = NumCont.ESTDELIVERDATETIME.min()
    report.STEPCHAIN[report.CONTAINER == cont] = NumCont.STEPCHAIN.min()
#������ �����
report.drop_duplicates(inplace=True)
#�������� stepchain ��� ����������� �� �������
custom = read_csv('d:/custom.csv')
report.STEPCHAIN[report.CONTAINER.isin(custom.CONTAINER)] = '�� �������'
#�������� stepchain ��� ����������� � ������� pending(����� �� ��� ����������)
pending = read_csv('d:/PendingShipment.csv', \
                usecols=['From', 'Container#', 'Est deliver'], \
                parse_dates=['Est deliver'], encoding='utf-16').\
        rename(columns={'Container#':'CONTAINER'})

pend_cont = pending.CONTAINER[pending.From == 'VIFS/VIFS RU CY (Vostochny)']
report.STEPCHAIN[report.CONTAINER.isin(pend_cont)] = '�� ���'
report.to_csv('Plan_sourse.csv', ';', index=False)
pendingDate = pending.groupby('CONTAINER').min().reset_index()
report = report.merge(pendingDate, 'left', on='CONTAINER')
report.ESTDELIVERDATETIME[report['Est deliver'].notnull()] = report['Est deliver']
report = report.drop(['Est deliver'], axis=1)
#�������� ������� � ������� ������
report.insert(5, 'week', [date.isocalendar(i)[1] \
                            for i in report.ESTDELIVERDATETIME])
#��������� ����
report.to_csv('Plan_sourse.csv', ';', index=False)

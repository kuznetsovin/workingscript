# -*- coding: utf-8 -*-
"""
Created on Thu Jul 18 12:35:08 2013

@author: IgKuznetsov
"""
from sqlalchemy import Column, String, create_engine, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import csv

engine = create_engine('oracle+cx_oracle://RC:tx8v64@LMTESTNW', echo=True)
Base = declarative_base()
class Artikul(Base):
    __tablename__ = 'art_igor'
    artikul = Column(String(255), primary_key=True)    
    
    def __init__(self, artikul):
        self.artikul = artikul
        
vgh = Table("logistic_units_view", Base.metadata, \
			autoload=True, autoload_with=engine)

Base.metadata.tables['art_igor'].drop(engine)
Base.metadata.tables['art_igor'].create(engine)
Session = sessionmaker(bind=engine)
session = Session()
f = open('d:/artikul.txt', 'rb')
importArt = f.read().splitlines()
f.close()
session.add_all(list(Artikul(art) for art in importArt))
session.commit()
queryVGH = session.query(vgh.c.id_produit, \
						vgh.c.poids/1000, \
						vgh.c.longueur*vgh.c.largeur*vgh.c.hauteur/1000000000, \
						vgh.c.id_ul).\
					join(Artikul, vgh.c.id_produit == Artikul.artikul).distinct()
					
queryLU2 = queryVGH.filter(vgh.c.id_ul == 'LU2')
lu2 = set([i[0] for i in queryLU2.distinct().all()])
queryLU1 = queryVGH.filter(vgh.c.id_ul == 'LU1', vgh.c.id_produit.notin_(lu2))
res = queryLU2.union_all(queryLU1).all()
f = open('vol_w.csv', 'wb')
out_csv = csv.writer(f, delimiter=";")
out_csv.writerows(res)
f.close()

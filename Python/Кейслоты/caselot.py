# -*- coding: utf-8 -*-

import cx_Oracle, win32com.client
from pandas import DataFrame
from pandas.io import sql
from pandas.core.common import notnull, isnull
from lib import ImportXLSInMaket
from datetime import datetime as dt

date_rep = dt.date(dt.today())
week_now = date_rep.isocalendar()[1]
#окрыть соединение с БД
db = cx_Oracle.Connection('RC', 'tx8v64', 'LMTESTNW')
rc = db.cursor()
FirstCollection = 11356
LastCollection = 11362
CalcReportFile = u'O:/Users2/Scm/ГАУЦП/Миша/КС/КЕЙСЛОТЫ на ФРЦ_расчет.xlsx'
setting = [[11356, 38],
           [11357, 15],
           [11358, 43],
           [11359, 16],
           [11360, 40],
           [11361, 15]]
set_col = DataFrame(setting, columns=['IDSEASON', 'WEEK'])
#для заданных коллекций выбрать информацию по кейслотам
rc.execute("TRUNCATE TABLE caselot_report")
rc.callproc("caselot_report_insrert_col", [FirstCollection, LastCollection])
db.commit()
print u'caselot load'
cs = sql.read_frame("SELECT * FROM caselot_report_preitog", db)
#вычисляем период ПРК
cs = cs.merge(set_col, 'left', on='IDSEASON')
max_dt_coll = cs.groupby('IDSEASON')['DATE_PRK'].max()
period_prk = []
for rec in cs.iterrows():
    ware = rec[1]
    if isnull(ware['NUM_WEEK']):
        period_prk.append(u'Без ПРК')
    else:
        if notnull(ware['WEEK']):
            if ware['NUM_WEEK'] < ware['WEEK']:
                period_prk.append(u'ПРК < %s недели' % str(int(ware['WEEK'])))
            else:
                year_active = dt.date(max_dt_coll[ware['IDSEASON']]).year
                if ware['DATE_PRK'].year < year_active:
                    period_prk.append(u'ПРК < %s недели' % str(int(ware['WEEK'])))
                else:
                    period_prk.append(u'ПРК %s неделя' % str(ware['NUM_WEEK']))
        else:
            prk_year = ware['DATE_PRK'].year
            if ware['NUM_WEEK'] < week_now and prk_year < date_rep.year:
                period_prk.append(u'ПРК < %s недели' % str(week_now))
            elif ware['NUM_WEEK'] >= week_now + 2 or prk_year > date_rep.year:
                if week_now + 2 > 52:
                    period_prk.append(u'ПРК >= %s недели' % str(week_now - 50))
                else:
                    period_prk.append(u'ПРК >= %s недели' % str(week_now + 2))
            else:
                period_prk.append(u'ПРК %s неделя' % str(ware['NUM_WEEK']))
cs.PERIOD_PRK = period_prk
cs = cs.drop('WEEK', axis=1)
cs.OTGR_CS[cs.OTGR_ED.isnull()] = cs.OTGR_ED[cs.OTGR_ED.isnull()] = 0          
#разбить общую выгрузку по коллекциям
for coll in xrange(FirstCollection, LastCollection + 1):
    #выгрузить ее в отчет на нужный лист
    SeasonCS = [list(rec[1]) for rec in cs[cs.IDSEASON == coll].iterrows()]
    ImportXLSInMaket(SeasonCS, CalcReportFile, str(coll))
ReportCSFile = \
    u'O:/Users2/Scm/ГАУЦП/Миша/КС/КЕЙСЛОТЫ_на_ФРЦ_%s_AW14_SS13_AW13_SS12_AW12.xlsx' % \
    date_rep.strftime('%d_%m_%Y')
#пересохранить расчетный файл с нужным именем
Excel = win32com.client.Dispatch("Excel.Application")
wb = Excel.Workbooks.Open(CalcReportFile)
Excel.Visible = 1
#обновить сводные в отчете
wb.RefreshAll()
wb.SaveAs(ReportCSFile, FileFormat=51)
wb.Save()

# -*- coding: cp1251 -*-

import win32com.client, os, sys

def ImportXLSInMaket(DataList, FileXLS, WorkSheet, BeginRowForInsertOffset=1):
    Excel = win32com.client.Dispatch("Excel.Application")
    if DataList is None:
        print u'��� ������ ��� ��������'
        sys.exit(0)
    elif os.path.exists(FileXLS) == False:
        print u'�� ������ ���� ��� ��������'
    else:
        wb = Excel.Workbooks.Open(FileXLS)
        ActiveSheet = wb.Sheets(WorkSheet)
                
    # �������� ������ ���������� ������� � ������
    BeginRow = ActiveSheet.UsedRange.Row
    BeginColumn = ActiveSheet.UsedRange.Column
                
    #�������� ���-�� ��������� ����� � ��������
    EndRow = len(ActiveSheet.UsedRange.Value2)
    EndColumn = len(ActiveSheet.UsedRange.Value2[0])

    #�������� ������ ������ ��� �������
    DataRow = BeginRow+BeginRowForInsertOffset 
    ActiveSheet.Range(ActiveSheet.Cells(DataRow, BeginColumn), \
                        ActiveSheet.Cells(EndRow, EndColumn)).ClearContents()             

    #�������� ���-�� �������� ��� �������
    EndColumn = len(DataList[0])
        
    for row in DataList:
        #��������� ������ �� ������
        ActiveSheet.Range(ActiveSheet.Cells(DataRow, BeginColumn), \
                        ActiveSheet.Cells(DataRow, EndColumn)).Value = row
        DataRow += 1
    wb.Save()
    wb.Close()
    print u'�������� ������� � ��������� ����'

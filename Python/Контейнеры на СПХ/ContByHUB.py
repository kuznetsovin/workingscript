# -*- coding: cp1251 -*- 

import wx
from pandas import DataFrame as df
from dateutil.parser import parse
from datetime import datetime as dt
from pandas.io import sql
from pandas import read_excel
import cx_Oracle, csv
import traceback, sys, math
 
#������ ������ ����������� �� ���
def FindContainerToHub():
    # ��������� ����� �� "Event for shipment" �� EEM
    EventForShipments = open("EventsForShipments.txt", "rb")
    
    # �������� ��� shipment'� �� �������� ���
    NOT_GMBH = ['NKM/NKM FI CY (Kotka)', \
                'LOGO/LOGO EE CY TLL (Tallinn)', \
                'VIFS/VIFS RU CY (Vostochny)', \
                '�����', \
    'TRXP/TRXP RU CY (Novorossiysk)']
    GMBH = ['ITLC/ITLC DE HUB (Hamburg)', \
            'B2B/B2B DE HUB (Hamburg)', \
            'B2B DE HUB (Hamburg)', \
            'ITL-Cargo GmbH', \
            'TVA/TVA UA CY (Riga)']
            
    PORTS = NOT_GMBH + GMBH
    tableCSV = csv.DictReader(EventForShipments, delimiter="\t")
    eemrep = [{'ActualDate':parse(rec['ActualDate'][:10], dayfirst=True), \
            'Shipment':rec['Shipment'], \
            'Destination':rec['Destination'], \
            'Carrier':rec['Carrier location'], \
            'Container':rec['Container'][:11]} \
            for rec in tableCSV \
                if (rec['Destination'] in NOT_GMBH and \
                    rec['Status'] == '92' and \
                    rec['Code'] == '2092') \
                    or (rec['Destination'] in GMBH and \
                        rec['Status'] in ('90', '92', '94') and \
                        rec['Code'] == '72100')]
    # �� ���� shipment'�� �� ���� 2 ���������� �����������
    for i in xrange(len(eemrep)):
        carrier_rec = eemrep[i]['Carrier']
        # ���� ���������� ����������    
        if 'SPORTMASTER' in carrier_rec and eemrep[i]['Carrier'] != "":
            eemrep[i]['Carrier'] = carrier_rec[:carrier_rec.find("/")].strip()  
        # ��������� Kuehne & Nagel
        elif 'K&N' in carrier_rec:
            eemrep[i]['Carrier'] = "Kuehne & Nagel"          
        # ��������� Expeditors International
        elif 'EI' in carrier_rec or eemrep[i]['Shipment'].startwith('EI'):
            eemrep[i]['Carrier'] = "Expeditors International"      
        # ��������� Panalpina
        elif 'ONLY)' in carrier_rec or eemrep[i]['Shipment'].startwith('PAC'):
            eemrep[i]['Carrier'] = "Panalpina"
        else:
            eemrep[i]['Carrier'] = ""      
    EvnShipment = df.from_dict(eemrep)
    EventForShipments.close()
    #�������� ������������ ����������� �� ��������
    true_cont_num = EvnShipment.Container.str.contains(r"[a-zA-z]{4}\d{7}")
    EvnShipment.Container[~(true_cont_num)] = EvnShipment.Shipment
    
    # �������� ������ �������� � �������� ����� �� shipment'�� �� ������ ���� 2
    db = cx_Oracle.Connection("RC", "tx8v64", "LMTESTNW")
    query = """select shipment, CAST(RTRIM(Sys_xmlagg(XMLELEMENT(col, tm||', ')).extract('/ROWSET/COL/text()').getclobval(), ', ') AS VARCHAR2(1000)) TM, SHIPTO 
                from (select distinct shipment, tm, SHIPTO from RC.EEM_SPECIFICATION_TABLE where shipment in  ('%s'))
                group by shipment, SHIPTO""" % \
                "','".join((row for row in EvnShipment['Shipment']))
    SipmentTM = sql.frame_query(query, db)
    
    # �������� � shipment'�� �� ���� 2 �������� ����� � ������ �������� �� ���� 3       
    AllShipment = EvnShipment.merge(SipmentTM, 'left', \
                                    left_on=['Shipment'], \
                                    right_on=['SHIPMENT'])  
      
    # ������� ���������� ���� 4, shipment'�, ������ � ��
    
    # ���������� �������� ����� � ���������� ����������� �� ���� 4, ����� ��������� ��������� �����������
    for rec in AllShipment.iterrows():
        AllShipment['TM'][rec[0]] = ",".join((str(row[1]['TM']) \
            for row in AllShipment.iterrows() \
                if row[1]['Container'] == rec[1]['Container']))
        AllShipment['TM'][rec[0]] = ",".join(sorted(set(AllShipment['TM'][rec[0]].split(","))))      
    # ������������ ������� ����������� �� ��� �� shipment'�� � ���� 6
    FinalContTabl = AllShipment.pivot_table(values=['ActualDate'], \
                                            rows=['Container', \
                                                    'TM', \
                                                    'Carrier',\
                                                    'Destination'], \
                                            aggfunc='min').reset_index()
   
    # �������� ���������� ������ �� Logistics
    query = """select container, CAST(RTRIM(Sys_xmlagg(XMLELEMENT(col, TM||', ')).extract('/ROWSET/COL/text()').getclobval(), ', ') AS VARCHAR2(1000)) TM, Destination, Carrier,   ActualDate
                 from (
                     select distinct L.VEHICLENUM container, C.TM , 'Logistics' Carrier, L.TOSTOR Destination, L.DDF ActualDate from RC.LOGISTICS_SPECIFICATION_TABLE l inner join catalog_ware c on L.MONIKER = c.ware 
                     where L.DDF >= trunc(sysdate - 30) and L.TITLEDOCSTATE = '�� ���' and L.TOSTOR in ('%s') and L.DESTPLACES in ('������, ��������','����')
                     ) 
                 group by container, Carrier, Destination, ActualDate""" % \
                 "','".join((row for row in PORTS))
    LogisticContainer = sql.frame_query(query, db)

    # �������� ���������� �� ��� �� Logistics c ������ ������� �� ����� ������
    LogisticContainer.rename(columns={'CONTAINER':'Container', \
                                  'TM':'TM', \
                                  'CARRIER':'Carrier', \
                                  'DESTINATION':'Destination', \
                                  'ACTUALDATE':'ActualDate'}, \
                            inplace=True)
    FinalContTabl = FinalContTabl.append(LogisticContainer)
    FinalContTabl.Destination = FinalContTabl.Destination.str.decode('cp1251')

    # ��������� ������� �� ��� = �������() - ActualDate ����������
    FinalContTabl.insert(len(FinalContTabl.columns), \
                        'IdleTime', \
                        [math.floor((dt.today()- date).days/7) \
                            for date in FinalContTabl['ActualDate']])
    
    # ��������� "����� � ��� (�����)"
    #OrderSPXFile = xls('OrderFromHUB.xlsx')
    OrderSPXFile = read_excel('OrderFromHUB.xlsx', 'Container')
    OrderSPX = OrderSPXFile.pivot_table(values=['OrderDate'], \
                                        rows=['Container'], aggfunc='min')
    
    # ��������� ������� �� ��� ����������� �� ���� 10
    result = FinalContTabl.set_index(['Container']).join(OrderSPX)
    
    #��������� � �������� ����
    result.TM = result.TM.str.decode('cp1251')
    result.to_excel("ConteinerToHub.xlsx", 
                    header=[u'����� ��������', \
                            u'����������', \
                            u'���', \
                            u'TM', \
                            u'�������', \
                            u'���� � ������'], 
                    index_label=[u'���������'])

#������ �������    
class MForm (wx.Frame):
    def __init__ (self, title):
        wx.Frame.__init__(self, None, title=title, size=(450, 130))
        self.SetBackgroundColour((255, 255, 255))
        font = wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD)
        wx.StaticText(self, label="1. ��������� ����� �� shipment'�� �� EEM", \
                    pos=(10, 10)).SetFont(font)
        wx.StaticText(self, 
                label="2. �������� ���������� �� ������ � ��� � OrderFromHUB.xlsx", \
                pos=(10, 30)).SetFont(font)
        wx.StaticText(self, label='3. ������� "�����"', \
                    pos=(10, 50)).SetFont(font)        
        btnOk = wx.Button(self, label="�����", pos=(10, 70), size=(80, -1))
        btnOk.SetFont(font) 
        btnOk.Bind(wx.EVT_BUTTON, self.onClickOK) 
        self.Show(True)

    def onClickOK(self, event):
        try:
            FindContainerToHub()
            wx.MessageBox("�������� ���� ConteinerToHub.xlsx �����������!", \
                        "���������", wx.OK)
        except Exception:        
            wx.MessageBox(traceback.format_exception(*sys.exc_info())[3], "������", wx.OK)

        

app = wx.App(False)
frame = MForm('���������� �� ���')
app.MainLoop()
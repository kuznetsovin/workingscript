Attribute VB_Name = "Module21"
Const Path As String = "O:\Users2\Scm\�����\����\������ �� PO � Shipments �� EEM\"


Sub ��������()
Dim SendTo As Variant
Dim SendToCopy As Variant
Dim MailText As String
Dim File As String

File = Path & "Shipments_" & Replace(CStr(Date), ".", "_")

'���������� �����
Call ���������(File, File & ".xls")

'������ ��������� ��������
SendTo = Array("yuki.hu@sportmaster-china.com", _
                "helen.sun@sportmaster-china.com", _
                "annie.wang@sportmaster-china.com", _
                "sally.liao@sportmaster-china.com", _
                "emma.zhang@ sportmaster - china.com")

'������ ����� ��������
SendToCopy = Array("Alexander V. Vinogradov/MILET/STILARSERVICE/07@STILARSERVICE", _
                    "Mariya A. Medvedeva/MILET/STILARSERVICE/07@STILARSERVICE", _
                    "Natalya V. Zonova/MILET/STILARSERVICE/07@STILARSERVICE", _
                    "Galina V. Kuzmina/MILET/STILARSERVICE/07@STILARSERVICE")

MailText = "Dear All," & Chr(13) _
            & Chr(13) _
            & "******************************************" & Chr(13) _
            & "Best regards," & Chr(13) _
            & "Kuznetsov Igor" & Chr(13) _
            & "Logistics Division" & Chr(13) _
            & "Sportmaster Group of Companies" & Chr(13) _
            & "tel.: +7(495)755-81-43 ext. 3661" & Chr(13) _
            & "******************************************"

'������ ��������
Call ��������_�����(SendTo, _
        SendToCopy, _
        "", _
        "Benchmark Report. FCL Shipments. " & CStr(Date), _
        MailText, _
        File & ".rar", _
        True)

End Sub

Sub ������������()
Attribute ������������.VB_ProcData.VB_Invoke_Func = " \n14"

    Workbooks.Open Path & "Shipments_" & Replace(CStr(Date), ".", "_") & ".xls"
    ' ��������� �����
    Call �������
    Call �����������������
    Call ��������_�������
    Call ����������������������
    
    ActiveWorkbook().Save
    MsgBox ("����� �����������!")

End Sub

Sub ��������_�������()
Dim RecCount As Integer
Dim Rec As Integer

    Range("A5").Select
    
    Selection.End(xlDown).Select
    Selection.End(xlToRight).Select
    RecCount = ActiveCell.Row
    
    For Rec = 6 To RecCount
        If Cells(Rec, 7).Value > 1 Then
            Cells(Rec, 7).Interior.Color = 65535
        End If
    Next Rec
End Sub

Sub ����������������������()

    Rows("1:2").Select
    Range("A2").Activate
    Selection.Delete Shift:=xlUp
    Range("A1").Select
    With ActiveSheet.PivotTables("��������������1").PivotFields( _
        "���������� �� ���� Actual Date")
        .Caption = " "
        .Function = xlMin
    End With
    Columns("A:G").Select
    Columns("A:G").EntireColumn.AutoFit
        Range("G4").Select
    With ActiveSheet.PivotTables("��������������1").PivotFields(" ")
        .NumberFormat = "DD.MM.YYYY"
    End With
    Columns("F:F").EntireColumn.AutoFit
    Range("H1").Select
End Sub

Sub �������()
    '��������� �������
    Range("A1").Select
    Range(Selection, Selection.End(xlDown)).Select
    Range(Selection, Selection.End(xlToRight)).Select
    Selection.AutoFilter
    
    '��������� �� EventStatus
    ActiveSheet.Range("A:AB").AutoFilter Field:=9, Criteria1:= _
        "ACTIVE"
    
    '��������� �� Status
    ActiveSheet.Range("A:AB").AutoFilter Field:=14, Criteria1:= _
        "Shipped"
    
    '��������� �� From
    ActiveSheet.Range("A:AB").AutoFilter Field:=15, Criteria1:= _
        "=*Office*", Operator:=xlAnd
        
    '��������� �� Destination
    ActiveSheet.Range("A:AB").AutoFilter Field:=16, Criteria1:=Array( _
        "B2B/B2B DE HUB (Hamburg)", "CRSTRD/CRSTRD UA CY (Odessa)", _
        "ITLC/ITLC DE HUB (Hamburg)", "LOGO/LOGO EE CY TLL (Tallin)", _
        "NKM/NKM FI CY (Kotka)", "VIFS/VIFS RU CY (Vostochny)", "FIT/FIT RU CY VVO (Vladivostok)", _
        "SDV/SDV DE HUB (Hamburg)", "CRSTRD/CRSTRD UA CY (Odessa)"), Operator:= _
        xlFilterValues
        
    '�������� ��������� �� ����� ����
    Range("A1").Select
    Range(Selection, Selection.End(xlDown)).Select
    Range(Selection, Selection.End(xlToRight)).Select
    Selection.Copy
    Sheets.Add After:=Sheets(Sheets.Count)
    ActiveSheet.Paste
    
    '����������� ��������� ����� �������
    Range("A2").Select
    Application.CutCopyMode = False
    Range(Selection, Selection.End(xlDown)).Select
    Range(Selection, Selection.End(xlToRight)).Select
    Application.CutCopyMode = False
    With Selection
        .VerticalAlignment = xlTop
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    Columns("A:A").Select
    Range(Selection, Selection.End(xlToRight)).Select
    Columns("A:AB").EntireColumn.AutoFit
    Sheets("����1").Select
    Sheets("����1").Name = "Source date"
    Sheets("Sheet1").Select
    Application.DisplayAlerts = False
    ActiveWindow.SelectedSheets.Delete
    Range("A1").Select
    Application.DisplayAlerts = True
End Sub

Sub �����������������()
    Dim RowCount As String
    
    '�������� �������
    Selection.End(xlDown).Select
    Selection.End(xlToRight).Select
    RowCount = CStr(ActiveCell.Row)
    Range(Selection, Selection.End(xlDown)).Select
    Range(Selection, Selection.End(xlToRight)).Select
    
    '������� ������� �� ����� �����
    Sheets.Add
    ActiveWorkbook.PivotCaches.Create(SourceType:=xlDatabase, SourceData:= _
        "Source date!R1C1:R" & RowCount & "C28", Version:=xlPivotTableVersion10).CreatePivotTable _
        TableDestination:="����2!R3C1", TableName:="��������������1", _
        DefaultVersion:=xlPivotTableVersion10
    Sheets("����2").Select
    Cells(3, 1).Select
    Sheets("����2").Select
    Sheets("����2").Name = "Summary table"
    
    '������� ������ ����
    ActiveSheet.PivotTables("��������������1").AddDataField ActiveSheet.PivotTables _
        ("��������������1").PivotFields("Actual Date"), _
        "���������� �� ���� Actual Date", xlCount
    With ActiveSheet.PivotTables("��������������1").PivotFields("Event Code")
        .Orientation = xlColumnField
        .Position = 1
    End With
    With ActiveSheet.PivotTables("��������������1").PivotFields("Event")
        .Orientation = xlColumnField
        .Position = 2
    End With
    With ActiveSheet.PivotTables("��������������1").PivotFields("Shipment")
        .Orientation = xlRowField
        .Position = 1
    End With
    With ActiveSheet.PivotTables("��������������1").PivotFields("Status")
        .Orientation = xlRowField
        .Position = 2
    End With
    With ActiveSheet.PivotTables("��������������1").PivotFields("From")
        .Orientation = xlRowField
        .Position = 3
    End With
    With ActiveSheet.PivotTables("��������������1").PivotFields("Destination")
        .Orientation = xlRowField
        .Position = 4
    End With
    ActiveWindow.SmallScroll Down:=8
    With ActiveSheet.PivotTables("��������������1").PivotFields("Carrier Location")
        .Orientation = xlRowField
        .Position = 5
    End With
    
    '��������� �����
    ActiveSheet.PivotTables("��������������1").PivotFields("Event Code").Subtotals _
        = Array(False, False, False, False, False, False, False, False, False, False, False, False)
    ActiveSheet.PivotTables("��������������1").PivotFields("Event").Subtotals = _
        Array(False, False, False, False, False, False, False, False, False, False, False, False)
    ActiveSheet.PivotTables("��������������1").PivotFields("Event Type").Subtotals _
        = Array(False, False, False, False, False, False, False, False, False, False, False, False)
    ActiveSheet.PivotTables("��������������1").PivotFields("Shipment").Subtotals = _
        Array(False, False, False, False, False, False, False, False, False, False, False, False)
    ActiveSheet.PivotTables("��������������1").PivotFields("Status").Subtotals = _
        Array(False, False, False, False, False, False, False, False, False, False, False, False)
    ActiveSheet.PivotTables("��������������1").PivotFields("From").Subtotals = _
        Array(False, False, False, False, False, False, False, False, False, False, False, False)
    ActiveSheet.PivotTables("��������������1").PivotFields("Destination"). _
        Subtotals = Array(False, False, False, False, False, False, False, False, False, False, _
        False, False)
    ActiveSheet.PivotTables("��������������1").PivotFields("Carrier Location"). _
        Subtotals = Array(False, False, False, False, False, False, False, False, False, False, _
        False, False)
    With ActiveSheet.PivotTables("��������������1")
        .ColumnGrand = False
        .RowGrand = False
    End With
End Sub



Attribute VB_Name = "import_txt"
Option Compare Database

Public Sub ImportOrderSpec()

Dim txtImport As String

Set fs = CreateObject("Scripting.FileSystemObject")
Set fImport = fs.OpenTextFile(CurrentProject.Path & "\output.txt", 2)

Set rsForImport = CurrentDb.OpenRecordset("ImportOrderSpec")

With rsForImport
    For Each fld In .Fields
        txtImport = txtImport & fld.Name & vbTab
    Next fld
    fImport.WriteLine Left(txtImport, Len(txtImport) - 1)

    Do While Not .EOF
        fImport.WriteLine ![Поле1] & vbTab & ![Поле2] & vbTab & ![Поле3]
        .MoveNext
    Loop
End With

rsForImport.Close
fImport.Close

MsgBox ("OrderSpec выгружен")
End Sub

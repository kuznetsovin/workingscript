CREATE OR REPLACE PROCEDURE RC.PF_UPDATE_MAIN_TABLE IS

BEGIN
insert into RC.PF_MAIN
select eem.NUM_PO,
        PF.COLLECTION,
        PF.TM,
        to_char(shdt.shippeddate, 'iW') week, 
        trunc(shdt.shippeddate, 'D') esd,
        PF.TYPE_TM,
        sum(eem.qty) kol,
        sum(eem.qty * VOL.VOLUME_ED) vol,
        case 
            when trunc(shdt.shippeddate, 'D') < PF.ESD - 7 then '���� ����� ���� ��������'
            when trunc(shdt.shippeddate, 'D') in (pf.esd - 7, pf.esd) then '���� � ���� ��������'
            when trunc(shdt.shippeddate, 'D') > pf.esd then '���� � ����������'
        end typ_pf,
        EEM.SHIPMENT  
    from RC.EEM_SPECIFICATION_TABLE eem 
        inner join RC.PF_MAIN pf on EEM.NUM_PO = pf.PO and pf.type_pf = '����'
        inner join RC.PF_OUTGOING_DATE shdt on eem.shipment = shdt.shipment and shdt.WORKSTATUS = 'New' 
        inner join PF_VOL_ED_PO vol on EEM.NUM_PO = vol.po  
    where EEM.shipto = '������, ��������' and 
        EEM.tostor in ('���� - ��� "��������� ������������� �������������� ������"','ITL-Cargo GmbH','B2B DE HUB (Hamburg)','�����','SDV Geis GmbH','(EEM) LOGO EE CY TLL (Tallinn)','(EEM) NKM CFS Kotka', '�������_������')              
    group by 
        eem.NUM_PO, 
        PF.COLLECTION,
        PF.TM,
        to_char(shdt.shippeddate, 'iW'), 
        trunc(shdt.shippeddate, 'D'),
        PF.TYPE_TM,
        case 
            when trunc(shdt.shippeddate, 'D') < PF.ESD - 7 then '���� ����� ���� ��������'
            when trunc(shdt.shippeddate, 'D') in (pf.esd - 7, pf.esd) then '���� � ���� ��������'
            when trunc(shdt.shippeddate, 'D') > pf.esd then '���� � ����������'
        end,
        EEM.SHIPMENT;
update RC.PF_OUTGOING_DATE set WORKSTATUS = 'Complite' where shipment in (select distinct shipment from PF_MAIN where shipment is not null);
commit;
END PF_UPDATE_MAIN_TABLE;
/
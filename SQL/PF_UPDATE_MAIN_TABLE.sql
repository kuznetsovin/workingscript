DROP PROCEDURE RC.PF_UPDATE_MAIN_TABLE;
CREATE PROCEDURE RC.PF_UPDATE_MAIN_TABLE IS

BEGIN

DELETE FROM pf_main pm WHERE pm.shipment IN (SELECT DISTINCT shipment FROM pf_outgoing_date_test WHERE workstatus = 'Shipped');

insert into RC.PF_MAIN
select eem.NUM_PO,
        PF.COLLECTION,
        PF.TM,
        to_char(shdt.shippeddate, 'iW') week, 
        shdt.shippeddate esd,
        PF.TYPE_TM,
        sum(eem.qty) kol,
        sum(eem.qty * VOL.VOLUME_ED) vol,
        case 
            when shdt.shippeddate <= PF.ESD - 14 then '���� ����� ���� ��������'
            when shdt.shippeddate BETWEEN pf.esd - 13 AND pf.esd then '���� � ���� ��������'
            when shdt.shippeddate > pf.esd then '���� � ����������'
        end typ_pf,
        EEM.SHIPMENT  
    from RC.EEM_SPECIFICATION_TABLE eem 
        inner join RC.PF_MAIN pf on EEM.NUM_PO = pf.PO and pf.type_pf = '����'
        inner join RC.PF_OUTGOING_DATE shdt on eem.shipment = shdt.shipment and shdt.WORKSTATUS in ('Shipped','OUOB') 
        inner join PF_VOL_ED_PO vol on EEM.NUM_PO = vol.po  
    where EEM.shipto = '������, ��������' and 
        EEM.tostor in ('���� - ��� "��������� ������������� �������������� ������"','ITL-Cargo GmbH','B2B DE HUB (Hamburg)','�����','SDV Geis GmbH','(EEM) LOGO EE CY TLL (Tallinn)','(EEM) NKM CFS Kotka', '�������_������', 'TRXP RU CY (Novorossiysk)')              
    group by 
        eem.NUM_PO, 
        PF.COLLECTION,
        PF.TM,
        to_char(shdt.shippeddate, 'iW'), 
        shdt.shippeddate,
        PF.TYPE_TM,
        case 
            when shdt.shippeddate < PF.ESD - 14 then '���� ����� ���� ��������'
            when shdt.shippeddate BETWEEN pf.esd - 13 AND pf.esd then '���� � ���� ��������'
            when shdt.shippeddate > pf.esd then '���� � ����������'
        end,
        EEM.SHIPMENT;
update RC.PF_OUTGOING_DATE_test set WORKSTATUS = 'Complite' where /*shipment in (select distinct shipment from PF_MAIN where shipment is not null) AND*/ workstatus = 'OUOB';

commit;
END PF_UPDATE_MAIN_TABLE;
/
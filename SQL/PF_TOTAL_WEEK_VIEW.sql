CREATE OR REPLACE VIEW RC.PF_TOTAL_WEEK_VIEW (
  COLLECTION,
  TM,
  NUM_WEEK,
  TYPE_TM,
  FACT_QTY,
  PALN_QTY,
  FACT_VOL,
  PALN_VOL,
  QTY_PR,
  VOL_PR
)
AS
  SELECT 
        p.COLLECTION,
        p.TM,p.NUM_WEEK,
        p.TYPE_TM, 
        sum(p.QTY) fact_qty, 
        pl.plan_qty, 
        sum(p.VOLUME) fact_vol, 
        pl.plan_vol, 
        sum(p.QTY)/pl.plan_qty qty_pr, 
        sum(p.VOLUME)/pl.plan_vol vol_pr
FROM RC.PF_MAIN P
    LEFT JOIN (SELECT 
                COLLECTION,
                TM,
                NUM_WEEK,
                TYPE_TM, 
                sum(QTY) plan_qty, 
                sum(VOLUME) plan_vol 
               FROM RC.PF_MAIN P 
               WHERE type_pf = '����' 
               GROUP BY COLLECTION,TM,NUM_WEEK,TYPE_TM) pl 
        ON p.COLLECTION =pl.COLLECTION AND p.TM=pl.TM AND p.NUM_WEEK = pl.NUM_WEEK AND p.TYPE_TM = pl.TYPE_TM
WHERE type_pf LIKE '%����%' 
GROUP BY 
    p.COLLECTION,
    p.TM,
    p.NUM_WEEK,
    p.TYPE_TM,
    pl.plan_qty,
    pl.plan_vol;
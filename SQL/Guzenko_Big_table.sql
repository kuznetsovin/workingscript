/*BEGIN
TRUNCATE TABLE art_igor;
INSERT INTO art_igor SELECT DISTINCT art FROM (
    SELECT DISTINCT "��� ������" art FROM stock_lm sl WHERE "Date_act" = '01.09.2013'
    UNION ALL
    SELECT DISTINCT idpr art FROM reports_rec_rep_repi rrrr WHERE dt >= '01.09.2012'
    ) A;
COMMIT;
END;*/
SELECT artikul,
    name_rus,
    e_tma_ware_group,
    e_tma_subdirection,
    e_tma_activity,
    e_tma_category,
    e_tma_ware_subgroup,
    name_tm,
    set_qte,
    decode(distr,'O','O`Stin','CM') biz,
    typename,
    sum(decode(rl.stock_status, 444, rl.qte,0)) otgruzka_mix,
    sum(decode(rl.stock_status, 444, 0, rl.qte)) otgruzka_ne_mix,
    sum(rrrr.qte) priemka,
    decode(lu2.id_ul, NULL, lu1.hauteur, lu2.hauteur) height_mm,
    decode(lu2.id_ul, NULL, lu1.largeur, lu2.largeur) width_mm,
    decode(lu2.id_ul, NULL, lu1.longueur, lu2.longueur) length_mm,
    decode(lu2.id_ul, NULL, lu1.poids, lu2.poids)/1000 weight_kg,
    sum(decode(sl.stock_status, 444, pik+kx+pal, 0)) stock_mix,
    sum(decode(sl.stock_status, 444, 0, pik+kx+pal)) stock_ne_mix
FROM art_igor ai 
    INNER JOIN catalog_ware_ cw ON artikul = ware
    LEFT JOIN type_preparation_lm7 tpl ON type_preparation = typeid
    LEFT JOIN reports_loadextd rl ON ware = idpr
    LEFT JOIN reports_rec_rep_repi rrrr ON ware = rrrr.idpr AND rrrr."TYPE" = 'priemka'
    LEFT JOIN rc.logistic_units_view lu1 ON ware = lu1.id_produit AND lu1.id_ul = 'LU1'
    LEFT JOIN rc.logistic_units_view lu2 ON ware = lu2.id_produit AND lu2.id_ul = 'LU2'
    LEFT JOIN stock_lm sl ON ware = "��� ������" AND "Date_act" = '01.09.2013'
WHERE client NOT IN (325,326,2642,2378,2366,2377,118,119,385)
GROUP BY artikul,
    name_rus,
    e_tma_ware_group,
    e_tma_subdirection,
    e_tma_activity,
    e_tma_category,
    e_tma_ware_subgroup,
    name_tm,
    set_qte,
    decode(distr,'O','O`Stin','CM'),
    typename,
    decode(lu2.id_ul, NULL, lu1.hauteur, lu2.hauteur),
    decode(lu2.id_ul, NULL, lu1.largeur, lu2.largeur),
    decode(lu2.id_ul, NULL, lu1.longueur, lu2.longueur),
    decode(lu2.id_ul, NULL, lu1.poids, lu2.poids)/1000
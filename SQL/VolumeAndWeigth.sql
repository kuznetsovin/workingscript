CREATE VIEW VolumeAndWeigth (
  ware,
  m3,
  kg
  )
AS 
SELECT DISTINCT luv.id_produit, hauteur*longueur*largeur/1000000000, poids/1000 
  FROM logistic_units_view luv 
    INNER JOIN art_igor ai ON luv.id_produit = ai.artikul 
  WHERE id_ul = 'LU2'
UNION
SELECT DISTINCT luv.id_produit, hauteur*longueur*largeur/1000000000, poids/1000 
  FROM logistic_units_view luv 
    INNER JOIN (SELECT artikul FROM art_igor ai
                MINUS
                (SELECT DISTINCT id_produit FROM logistic_units_view luv INNER JOIN art_igor ai ON luv.id_produit = ai.artikul WHERE id_ul = 'LU1'
                INTERSECT
                SELECT DISTINCT id_produit FROM logistic_units_view luv INNER JOIN art_igor ai ON luv.id_produit = ai.artikul WHERE id_ul = 'LU2')) art ON luv.id_produit = art.artikul
  WHERE id_ul = 'LU1'

CREATE VIEW Dvig_Tovar3mon(
  idpr,
  mes,
  TYPE,
  qty
  )
  AS
SELECT idpr, trunc(dt,'MM'), TYPE, sum(qte) 
  FROM reports_rec_rep_repi rrrr 
    INNER JOIN art_igor ai ON rrrr.idpr = ai.artikul
  WHERE TYPE IN ('Otgruzka','Priemka') AND dt >= add_months(trunc(sysdate,'MM'),-3)
  GROUP BY idpr, trunc(dt,'MM'), TYPE
SELECT dt, ware, tm, typepost, vol * krat vol, vol_1 * krat vol_1,
       stock_vol * krat stock, vol_shipped * krat vol_shipped
  FROM (SELECT DISTINCT p.datesales dt, cw.ware, cw.name_tm tm,
                        p.kolvo_otgruz vol, p.kolvo_zavozf vol_1,
                        NVL (stock.vol, 0) stock_vol, pz.typepost,
                        CASE
                           WHEN x = 0 OR x IS NULL
                              THEN 1
                           ELSE x
                        END krat, NVL (extd_fact.qte_fact, 0) qte_fact,

                        NVL (extd_fact.qte_fact, 0) vol_shipped
                   FROM rc.prk p LEFT JOIN stock_lm_now stock
                        ON stock.art = p.ware
                        --�������� � ��� �� ��
                        LEFT JOIN
                        (SELECT   r.idpr, SUM (r.qte) qte_fact
                             FROM rc.reports_loadextd_xtd r
                            WHERE r.dt > '26.11.2012' AND r.pz IS NOT NULL
                         GROUP BY r.idpr) extd_fact ON p.ware = extd_fact.idpr
                                                --���������� �������� �� ��������
                        LEFT JOIN
                        (SELECT cw.ware, cw.name_tm,
                                CASE
                                   WHEN cw.kind = 'U'
                                      THEN cw.set_qte
                                   ELSE 1
                                END x
                           FROM catalog_ware_ cw) cw ON cw.ware = p.ware
                        LEFT JOIN
                        
                        -- ��� ���������� (������� - ���������)
                        (SELECT DISTINCT pz_all.typepost, pz_all.ware
                                    FROM pz_all
                                   WHERE pz_all.rasprcenter = '���'
                                     AND pz_all.idseason = 11359) pz
                        ON pz.ware = p.ware
                  WHERE p.idseason = 11359            --AND p.divrealiz = '��'
                    AND p.datesales <= TRUNC (SYSDATE, 'D') - 1
                    AND p.kolvo_zavozf > 0);
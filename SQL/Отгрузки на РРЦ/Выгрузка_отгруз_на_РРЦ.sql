SELECT a.type, a.dt, 
       DECODE(a.client,927, 827, 467, 837, 427, 847, 797, 857, a.client) end client, 
       case a.client_type WHEN 'Ostin' THEN 'OST' ELSE '��' END client_type, a.qte, a.cntcor,
       a.cubature, a.pik_kor, a.tp, 
       case WHEN a.pz is not null THEN '��' END AS pz
  FROM reports_loadextd_group a
  WHERE a.dt between '20.08.2013' AND TRUNC(SYSDATE)-1 
    AND a.TYPE = 'Otgruzka' 
    AND a.client in (437,427,467,797,927,937,947,957,827,807,277, 837,847,857)
    AND a.pik_kor IS NOT NULL

SELECT a.dt, a.client, case a.client_type WHEN 'Ostin' THEN 'OST' ELSE '��' END client_type, sum(a.qte), sum(a.cntcor)
  FROM reports_loadextd_group a
  WHERE a.dt between TRUNC(SYSDATE)-7 AND TRUNC(SYSDATE)-1  
    AND a.TYPE = 'Otgruzka' 
    AND a.pik_kor IS NOT NULL
    GROUP BY a.dt, a.client, a.client_type

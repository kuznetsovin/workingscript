DROP VIEW RC.VGH_ED_WITH_KRAT_BX;

CREATE OR REPLACE FORCE VIEW RC.VGH_ED_WITH_KRAT_BX
(
   ID_PRODUIT,
   KR,
   VKOR,
   V,
   POIDS,
   LONGUEUR,
   LARGEUR,
   HAUTEUR
)
AS
   SELECT   DISTINCT gb.id_produit,
                     kr_kor.kr,
                     kr_kor.vkor,
                     gb.v,
                     gb.poids,
                     gb.longueur,
                     gb.largeur,
                     gb.hauteur
     FROM      (SELECT   l.id_produit,
                         l.longueur * l.largeur * l.hauteur / 1000000000 v,
                         l.poids,
                         l.longueur,
                         l.largeur,
                         l.hauteur
                  FROM      rc.logistic_units_view l
                         INNER JOIN
                            art_igor a
                         ON a.artikul = l.id_produit
                 WHERE   l.id_ul = 'LU2'
                UNION
                (SELECT   l.id_produit,
                          l.longueur * l.largeur * l.hauteur / 1000000000 v,
                          l.poids,
                          l.longueur,
                          l.largeur,
                          l.hauteur
                   FROM      rc.logistic_units_view l
                          INNER JOIN
                             art_igor a
                          ON a.artikul = l.id_produit
                  WHERE   l.id_ul = 'LU1'
                 MINUS
                 SELECT   l.id_produit,
                          l.longueur * l.largeur * l.hauteur / 1000000000 v,
                          l.poids,
                          l.longueur,
                          l.largeur,
                          l.hauteur
                   FROM      rc.logistic_units_view l
                          INNER JOIN
                             art_igor a
                          ON a.artikul = l.id_produit
                  WHERE   l.id_ul = 'LU2')) gb
            LEFT JOIN
               (SELECT   l.id_produit,
                         L.QTE * DECODE (SET_QTE, 0, 1, SET_QTE) kr,
                         l.longueur * l.largeur * l.hauteur / 1000000000 vkor
                  FROM         rc.logistic_units_view l
                            INNER JOIN
                               art_igor a
                            ON a.artikul = l.id_produit
                         INNER JOIN
                            catalog_ware cw
                         ON L.ID_PRODUIT = CW.WARE
                 WHERE   l.id_ul = 'BX') kr_kor
            ON gb.id_produit = kr_kor.id_produit
    WHERE   gb.longueur > 1;

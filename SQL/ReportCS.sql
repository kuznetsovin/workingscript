--aw12 = 11356
--ss12 = 11357

SELECT my1.ware, my1.kolvo_zakup, my1.kolvo_zakup*my1.set_qte as kolvo_zakup_ed, my1.DATEPOST_START, my1.DATEPRIH_FINISH, dpf.dt_prih,
'' as period_prk,
my.datesales as date_prk, to_number(to_char(my.datesales,'iw')) as num_week, my1.divzakup, my1.IDSEASON, my1.SET_QTE, my1.SET_ARTS,
decode(my2.pik,null,0,my2.pik) as pik, decode(my2.kx, null,0,my2.kx) as kx, decode((my2.pik+my2.kx),null,0,(my2.pik+my2.kx)) as stock_cs,
decode(my2.bx,null,0,my2.bx) as bx, decode(my2.pik,null,0,my2.pik*my1.set_qte) as pik_ed, decode(my2.kx, null,0,my2.kx*my1.set_qte) as kx_ed, 
decode(( my2.pik+my2.kx),null,0,(my2.pik+my2.kx)*my1.set_qte) as stock_ed, 
decode(my3.priem, null,0,my3.priem) as priem_cs, decode(my3.priem,null,0,my3.priem*my1.set_qte) as priem_ed, 
decode(my4.otgr,null,0,my4.otgr) as otgr_cs, decode(my4.otgr,null,0,my4.otgr*my1.set_qte) as otgr_ed    
from 
(select T.WARE,  min(T.DATESALES) as DATESALES from RC.PRK t, RC.CATALOG_WARE t1
where t1.ware(+)=T.WARE 
and T.IDSEASON=11357
and T.RASPRCENTER='���'
and T1.KIND='U'
group by T.WARE) my,

(select T.WARE, T.DIVZAKUP, min(T.DATEPOST_START) DATEPOST_START, min(T.DATEPRIH_FINISH) DATEPRIH_FINISH, SUM(T.KOLVO_ZAKUP) as KOLVO_ZAKUP,T1.SET_QTE, T1.SET_ARTS,T.IDSEASON 
from RC.PZ_ALL t, RC.CATALOG_WARE t1
where t1.ware(+)=T.WARE  
and T.RASPRCENTER='���'
and T.IDSEASON=11357
and T1.KIND='U'
group by T.WARE, T.DIVZAKUP, T1.SET_QTE, T1.SET_ARTS,T.IDSEASON ) my1,

(select T."��� ������" as ware, sum(T.PIK) as pik, sum(T.KX) as kx, sum(T.KX_BX+T.PAL_BX) as BX from RC.STOCK_LM t
where t."Date_act"=trunc(sysdate)
and T."��� ������" in (select distinct T.WARE from RC.PZ_ALL t, RC.CATALOG_WARE t1
where t1.ware(+)=T.WARE  
and T.RASPRCENTER='���'
and T.IDSEASON=11357
and T1.KIND='U') 
and t."Type_zone" in ('���','���')
group by T."��� ������") my2,

(select T.IDPR, sum(T.QTE) priem from RC.REPORTS_REC_REP_REPI t 
where T.IDPR in (select distinct T.WARE from RC.PZ_ALL t, RC.CATALOG_WARE t1
where t1.ware(+)=T.WARE  
and T.RASPRCENTER='���'
and T.IDSEASON=11357
and T1.KIND='U') 
and t.type='priemka'
group by T.IDPR ) my3,

(select T.IDPR, sum(T.QTE) otgr from RC.REPORTS_REC_REP_REPI t 
where T.IDPR in (select distinct T.WARE from RC.PZ_ALL t, RC.CATALOG_WARE t1
where t1.ware(+)=T.WARE  
and T.RASPRCENTER='���'
and T.IDSEASON=11357
and T1.KIND='U') 
and t.type='Otgruzka'
group by T.IDPR ) my4,

(SELECT min(a.dt) AS dt_prih, a.idpr
  FROM reports_rec_rep_repi a, catalog_ware b
  where a.idpr=b.ware and a.type='priemka' and a.dt>='01.05.2010' and b.set_qte>0
  group by a.idpr) dpf

where my.ware(+)=my1.ware
and my2.ware(+)=my1.ware
and my3.idpr(+)=my1.ware
and my4.idpr(+)=my1.ware
and dpf.idpr(+)=my1.ware

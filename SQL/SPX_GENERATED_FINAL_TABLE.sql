begin
	
CREATE OR REPLACE PROCEDURE RC.SPX_GENERATED_FINAL_TABLE (ORDERDATE IN DATE) IS
BEGIN
INSERT INTO SPX_FINAL_TABL VALUE
         SELECT r.SHIPMENT, 
             r.CONTAINER, 
             r.POTOK,               
             r.ware,
			 r.EstDeliver, 
             r.QTY, 
             r.TM, 
             r.STOCK, 
             (r.EDD-r.TODAY_WEEK)/7 AS week_delay, 
             decode(r.EDD,
                     null,'No PRK/EDD',
                     case 
                         when r.EDD<r.TODAY_WEEK then 'expired prk/EDD'
                         when r.EDD=r.TODAY_WEEK then 'actual PRK/EDD'
                         when r.EDD-r.TODAY_WEEK=7 then 'actual+1 PRK/EDD'
                         when r.EDD-r.TODAY_WEEK=14 then 'actual+2 PRK/EDD'
                         else 'future PRK/EDD'
                      end
                  ) AS CategoryEDD, 
             r.QTY/sc.ContQTY AS procent, 
             Decode(tgvs.TG_VS,null,DECODE(TPL.TYPEID,Null,'���',TPL.TYPEID),'��') AS tp, 
             r.FROMSTOR, r.TOSTOR, r.DOC_STATUS
         FROM (SELECT sd.SHIPMENT, 
                 sd.CONTAINER,
                 sd.POTOK,
                 sd.ware,
                 sd.EstDeliver,
                 sd.QTY,
                 sd.TM, 
                 to_date(decode(sd.ENDDELIVERDATE,null,null,sd.ENDDELIVERDATE+7-TO_CHAR(sd.ENDDELIVERDATE,'D'))) AS EDD, 
                 ORDERDATE+7-TO_CHAR(ORDERDATE,'D') AS TODAY_WEEK, 
                 stk.KOL AS STOCK, cw.TRADEGROUP, cw.TYPE_PREPARATION, sd.FROMSTOR, sd.TOSTOR, sd.DOC_STATUS
             FROM SPX_SOURCE_DATE sd 
                 LEFT JOIN RC.STOCK_TODAY stk ON sd.ware=stk."��� ������" 
                 INNER JOIN catalog_ware cw ON sd.ware = cw.WARE) r 
             INNER JOIN (SELECT sd.CONTAINER, Sum(sd.QTY) AS ContQTY FROM SPX_SOURCE_DATE sd GROUP BY sd.CONTAINER) sc ON r.CONTAINER=sc.CONTAINER 
             LEFT JOIN RC.TYPE_PREPARATION_LM7 tpl ON r.TYPE_PREPARATION=TPL.TYPEID 
             LEFT JOIN RC.SPX_TRADEGROUP_VS tgvs ON r.TRADEGROUP=TGVS.TG_VS;
END SPX_GENERATED_FINAL_TABLE;

CREATE OR REPLACE PROCEDURE RC.SPX_GENERATE_SOURCEDATE (HUB in VARCHAR, SKLAD in VARCHAR, COLLECTION in VARCHAR) IS
BEGIN
INSERT INTO SPX_SOURCE_DATE value
SELECT EEM.SHIPMENT, 
            EEM.CONTAINER, 
            EEM.WARE, 
            EEM.ESTDELIVERDATETIME AS EstDeliver, 
            EEM.ENDDELIVERDATE, 
            EEM.NUM_PO, 
            EEM.QTY, 
            EEM.TM, 
            EEM.TOSTOR, 
            EEM.FROMSTOR, 
            EEM.DOC_STATUS, 
            'EEM' AS POTOK
        FROM RC.EEM_SPECIFICATION_TABLE EEM 
            INNER JOIN (SELECT DISTINCT th.SHIPMENT 
                        FROM (SELECT to_char(cont.NUM_PO || Replace(cont.CONTAINER,' ','')) KeyPOCnt, cont.SHIPMENT 
                                FROM rc.eem_specification_table cont 
                                    INNER JOIN RC.SPX_PORTS port ON cont.tostor=PORT.DESTINATION and PORT.PORT = HUB and shipto = SKLAD) th 
                            LEFT JOIN (SELECT to_char(cont.NUM_PO || Replace(cont.CONTAINER,' ','')) KeyPOCnt, cont.SHIPMENT 
                                        FROM rc.eem_specification_table cont 
                                            INNER JOIN RC.SPX_PORTS port ON cont.fromstor=PORT.DESTINATION and PORT.PORT = HUB and shipto = SKLAD) fh 
                                       ON TH.KeyPOCnt=FH.KeyPOCnt 
                        WHERE FH.KeyPOCnt IS NULL) ZTMP ON EEM.SHIPMENT = ZTMP.SHIPMENT
        WHERE EEM.STATUSDATETIME >= ADD_MONTHS(TRUNC(SYSDATE,'MM'), -4)
        UNION ALL
        SELECT to_char(LOGIST.DOCID), 
            DECODE(LOGIST.VEHICLENUM,'����',DECODE(LOGIST.EINGANG,Null,'TRUCK',LOGIST.EINGANG),'TRUCK',DECODE(LOGIST.EINGANG,Null,'TRUCK',LOGIST.EINGANG),LOGIST.VEHICLENUM) CONT,
            LOGIST.MONIKER, 
            LOGIST.DDP,           
            prk_l.DATE_1-7 EDD,
            '' NUM_PO,
            LOGIST.ARTQTOTAL,
            cw.TM,
            LOGIST.TOSTOR,
            LOGIST.FROMSTOR,  
            LOGIST.TITLEDOCSTATE, 
            'Logistics' AS POTOK
        FROM RC.LOGISTICS_SPECIFICATION_TABLE LOGIST
            INNER JOIN RC.SPX_PORTS port ON LOGIST.TOSTOR=PORT.DESTINATION and PORT.PORT = HUB AND LOGIST.DESTPLACES= SKLAD and LOGIST.TITLEDOCSTATE in ('� ����','�� ���') 
            INNER JOIN catalog_ware cw ON LOGIST.MONIKER = cw.WARE 
            LEFT JOIN RC.PRK_GR_DIV_ALL_SEASON_2 prk_l ON LOGIST.MONIKER = prk_l.ART and prk_l.season  = COLLECTION;
END SPX_GENERATE_SOURCEDATE;

end;
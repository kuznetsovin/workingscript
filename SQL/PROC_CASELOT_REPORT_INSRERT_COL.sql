CREATE OR REPLACE PROCEDURE RC.caselot_report_insrert_col (FirstColl in INTEGER, LastColl in INTEGER)
AS
BEGIN

FOR col IN FirstColl .. LastColl LOOP
  INSERT INTO caselot_report VALUE
    SELECT 
        pa.WARE, 
        pa.KOLVO_ZAKUP, 
        pa.KOLVO_ZAKUP*cw.SET_QTE as kolvo_zakup_ed, 
        min(pa.DATEPOST_START) AS DATEPOST_START, 
        min(pa.DATEPRIH_FINISH) AS DATEPRIH_FINISH, 
        '' as period_prk, 
        min(p.DATESALES) as date_prk, 
        to_number(to_char(min(p.DATESALES),'iw')) as num_week, 
        pa.DIVZAKUP, 
        pa.idseason, 
        cw.SET_QTE, 
        cw.set_arts,
      	decode(sum(sl.PIK),null,0,sum(sl.PIK)) AS pik, 
      	decode(sum(sl.KX), null,0,sum(sl.KX)) AS kx, 
      	decode((sum(sl.PIK)+sum(sl.KX)),null,0,(sum(sl.PIK)+sum(sl.KX))) AS stock_cs,  
      	decode(sum(sl.KX_BX+sl.PAL_BX),null,0,sum(sl.KX_BX+sl.PAL_BX)) AS bx, 
      	decode(sum(sl.PIK),null,0,sum(sl.PIK)*cw.SET_QTE) AS pik_ed, 
      	decode(sum(sl.KX), null,0,sum(sl.KX)*cw.SET_QTE) AS kx_ed, 
  	    decode((sum(sl.PIK)+sum(sl.KX)),null,0,(sum(sl.PIK)+sum(sl.KX))*cw.SET_QTE) AS stock_ed 
     FROM prk p
        INNER JOIN catalog_ware cw ON p.ware=cw.ware AND p.IDSEASON=col AND p.RASPRCENTER='���' AND cw.KIND='U'
        INNER JOIN pz_all pa ON pa.ware = cw.ware AND pa.IDSEASON=col AND pa.RASPRCENTER='���' AND cw.KIND='U'
        LEFT JOIN stock_lm sl ON sl."��� ������" = cw.ware AND sl.stock_status <> 444 AND sl."Date_act" = trunc(sysdate) AND sl."Type_zone" = '���'
     GROUP BY 
        pa.ware,
        pa.kolvo_zakup,  
        pa.DIVZAKUP, 
        cw.SET_QTE, 
        cw.SET_ARTS,
        pa.IDSEASON;

END LOOP;
END;
/
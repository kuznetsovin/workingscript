CREATE OR REPLACE VIEW RC.STOCKLM7 (
  "��� ������",
  ED,
  EDKOR,
  EDPAL,
  KOR,
  PAL,
  MIXKOR,
  DLINA,
  SHIRINA,
  VISOTA,
  VES,
  SEASON,
  TM
)
AS
  SELECT a."��� ������", 
sum(a.pik*decode(b.set_qte,0,1,null,1,b.set_qte)) ed, 
sum(A.KX *decode(b.set_qte,0,1,null,1,b.set_qte)) edkor, 
sum(a.pal*decode(b.set_qte,0,1,null,1,b.set_qte)) edpal,
sum(A.KX_BX *decode(b.set_qte,0,1,null,1,b.set_qte)) kor, 
sum(a."PAL_pal"*decode(b.set_qte,0,1,null,1,b.set_qte)) pal, 
case a.stock_status when 444 then 'mixkor' else 'kor' end mixkor,
case a."Type_zone" 
    when '���' then t_mgt.d
    when '���' then t_kgt.d
    end dlina,
 case a."Type_zone" 
    when '���' then t_mgt.s
    when '���' then t_kgt.s
    end shirina,
 case a."Type_zone" 
    when '���' then t_mgt.h
    when '���' then t_kgt.h
    end visota,
 case a."Type_zone" 
    when '���' then t_mgt.w
    when '���' then t_kgt.w
    end ves,
a.season, b.tm 
FROM stock_lm a 
    inner join catalog_ware b on a."��� ������" =b.ware
    left join (select L.ID_PRODUIT, max(L.LONGUEUR) d, max(L.LARGEUR) s, max(L.HAUTEUR) h, max(L.POIDS) w from RC.LOGISTIC_UNITS_VIEW l where id_ul = 'LU2' group by L.ID_PRODUIT) t_mgt on B.WARE = t_mgt.ID_PRODUIT
    left join (select L.ID_PRODUIT, max(L.LONGUEUR) d, max(L.LARGEUR) s, max(L.HAUTEUR) h, max(L.POIDS) w from RC.LOGISTIC_UNITS_VIEW l where id_ul = 'LU1' group by L.ID_PRODUIT) t_kgt on B.WARE = t_kgt.ID_PRODUIT
where a."Type_zone" in ('���','���')
and a."Date_act" =to_char(SYSDATE, 'DD.MM.YYYY') and a.stock_status<>1 
group by a."��� ������", a.season, b.tm,
case a.stock_status when 444 then 'mixkor' else 'kor' end,
case a."Type_zone" 
    when '���' then t_mgt.d
    when '���' then t_kgt.d
    end,
 case a."Type_zone" 
    when '���' then t_mgt.s
    when '���' then t_kgt.s
    end,
 case a."Type_zone" 
    when '���' then t_mgt.h
    when '���' then t_kgt.h
    end,
 case a."Type_zone" 
    when '���' then t_mgt.w
    when '���' then t_kgt.w
    end;
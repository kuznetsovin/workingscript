INSERT INTO order_dynamic VALUE
SELECT 
  dt_order,
  w_name,
  tn_name,
  lgn_code,
  decode(o_type, '����� �� ��������� �����', 'Alloc', 'Repl'),
  tpl.typename,
  count(ware),
  sum(confirmed_item_count*decode(cw.set_qte,0,1,set_qte)),
  sum(reserved_volume),
  decode(tpl.typename, 
        '���',case 
                    when type_preparation IN ('31','32') then '����' /*E_TMA_CATEGORY = '����������' then '����������'*/
                    when E_TMA_WARE_SUBGROUP in ('����-���������','������� �������','�������������','��������','�������������','��������� ���','������ � ������','���������� ���','�������') then '���������'
                    when E_TMA_SUBDIRECTION = '������' then '������'
                    when E_TMA_ACTIVITY = '������' then '������'
               else '�c�������' end,
        ''),
  cd.region
FROM sm_rms_for_lm7.v_ord_item_report@rms rms
  INNER JOIN catalog_ware cw ON rms.product_code = cw.ware
  INNER JOIN type_preparation_lm7 tpl ON decode(rms.preparation_type,
                                                NULL, decode(cw.type_preparation,
                                                              NULL, decode(lf_name, 
                                                                          '��������','18',
                                                                          '�������','18',
                                                                          '������������ ���������', '18',
                                                                          '�������� ������������', '18',decode(cw.kind,'C','31','3')),                          
                                                              cw.type_preparation),
                                                rms.preparation_type) = tpl.typeid
  LEFT JOIN client_dict cd ON rms.lgn_code = cd.clientid
WHERE dt_order = '29.08.2013' AND confirmed_item_count > 0
GROUP BY   
  dt_order,
  w_name,
  tn_name,
  lgn_code,
  decode(o_type, '����� �� ��������� �����', 'Alloc', 'Repl'),
  tpl.typename,
  decode(tpl.typename, 
        '���',case 
                    when type_preparation IN ('31','32') then '����' /*E_TMA_CATEGORY = '����������' then '����������'*/
                    when E_TMA_WARE_SUBGROUP in ('����-���������','������� �������','�������������','��������','�������������','��������� ���','������ � ������','���������� ���','�������') then '���������'
                    when E_TMA_SUBDIRECTION = '������' then '������'
                    when E_TMA_ACTIVITY = '������' then '������'
               else '�c�������' end,
        ''),
  cd.region
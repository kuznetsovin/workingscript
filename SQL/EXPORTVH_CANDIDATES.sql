CREATE VIEW EXPORTVH_CANDIDATES (
  ware,
  ed,
  pal,
  pal_type, 
  krat, 
  koef, 
  lenght,
  width,
  height, 
  weight,
  e_tma_ware_group,	
  e_tma_subdirection,	
  e_tma_activity,	
  e_tma_category,	
  e_tma_ware_subgroup
  )
  AS 
SELECT sl."��� ������", pal ed, "PAL_pal" pal,
  ppok.pal_type, ppok.krat, ppok.koef, 
  longueur/10 lenght,largeur/10 width,hauteur/10 height, poids/1000,
  e_tma_ware_group,	e_tma_subdirection,	e_tma_activity,	e_tma_category,	e_tma_ware_subgroup
  FROM stock_lm sl
    INNER JOIN pallet_params_of_kratn ppok ON sl."��� ������"=ppok.idpr
    INNER JOIN logistic_units_view luv ON ppok.idpr = luv.id_produit AND ppok.pal_type = luv.id_emballge
    INNER JOIN catalog_ware cw ON luv.id_produit = cw.ware
  WHERE "Date_act"=trunc(sysdate) AND stock_status = 0
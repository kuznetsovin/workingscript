CREATE OR REPLACE VIEW RC.PLAN_INCOMING (
  STEPCHAIN,
  FROMSTOR,
  CONTAINER,
  SHIPMENT,
  TM,
  TYPENAME,
  ESTDELIVERDATETIME
)
AS
  select distinct '�� ���' stepchain, E.FROMSTOR, upper(e.CONTAINER) container, e.shipment, E.TM, case when C.WARE in ('BW51048',
'N2127','N2144','N2211L-L','N2211L-R','N2211M-L','N2211M-R','N2212L-L','N2212L-R','N2213L-L','N2213L-R','N2221M','N2222L-L','N2222L-R','N2222M-L','N2222M-R','N2223L-R','N2223M-R',
'N2224L-L','N2224L-R','N2224M-L','N2224M-R','N2224XL-L','N2224XL-R','N2225L-L','N2225L-R','N2225M-L','N2225M-R','N2225XL-L','N2225XL-R','N2226L-L','N2226L-R','N2229M','N2310-L','N2310-R','N2402','VD58426NP',
'VD58876','VD58890','VD68797') then '���' else T.TYPENAME end as TYPENAME, E.ESTDELIVERDATETIME 
from RC.EEM_SPECIFICATION_TABLE e
    inner join RC.CATALOG_WARE c on e.ware = C.WARE
    left join RC.TYPE_PREPARATION_LM7 t on C.TYPE_PREPARATION = T.TYPEID   
where E.SHIPTO = '������, ��������' and E.SHIPTO = E.TOSTOR and status = 90 
    and E.FROMSTOR in ('���� - ��� "��������� ������������� �������������� ������"','ITL-Cargo GmbH','B2B DE HUB (Hamburg)','�����','SDV Geis GmbH','(EEM) LOGO EE CY TLL (Tallinn)','(EEM) NKM CFS Kotka','TRXP RU CY (Novorossiysk)')
    AND e.enddeliverdate >= trunc(sysdate - 75)
union all
--����-� �� ���� eem
select distinct DECODE(status, 90, '���� � ����', '� �����'), E.TOSTOR, upper(e.CONTAINER), e.shipment, E.TM, case when C.WARE in ('BW51048',
'N2127','N2144','N2211L-L','N2211L-R','N2211M-L','N2211M-R','N2212L-L','N2212L-R','N2213L-L','N2213L-R','N2221M','N2222L-L','N2222L-R','N2222M-L','N2222M-R','N2223L-R','N2223M-R',
'N2224L-L','N2224L-R','N2224M-L','N2224M-R','N2224XL-L','N2224XL-R','N2225L-L','N2225L-R','N2225M-L','N2225M-R','N2225XL-L','N2225XL-R','N2226L-L','N2226L-R','N2229M','N2310-L','N2310-R','N2402','VD58426NP',
'VD58876','VD58890','VD68797') then '���' else T.TYPENAME end, E.ESTDELIVERDATETIME 
from RC.EEM_SPECIFICATION_TABLE e
    inner join RC.CATALOG_WARE c on e.ware = C.WARE
    left join RC.TYPE_PREPARATION_LM7 t on C.TYPE_PREPARATION = T.TYPEID
    --������ PO-��������� ��� ����������� ������������� ����-��
    left join (select distinct concat(Em.NUM_PO, Em.CONTAINER) idrow from RC.EEM_SPECIFICATION_TABLE em where em.SHIPTO = '������, ��������' and em.SHIPTO = em.TOSTOR) fh on concat(E.NUM_PO, E.CONTAINER) = fh.idrow   
where E.SHIPTO = '������, ��������' and 
    E.TOSTOR in ('���� - ��� "��������� ������������� �������������� ������"','ITL-Cargo GmbH','B2B DE HUB (Hamburg)','�����','SDV Geis GmbH','(EEM) LOGO EE CY TLL (Tallinn)','(EEM) NKM CFS Kotka','TRXP RU CY (Novorossiysk)') 
    and fh.idrow is NULL AND e.enddeliverdate >= trunc(sysdate - 75)
union all
--��������� �� ���
select distinct '�� ���', l.FROMSTOR, DECODE(l.VEHICLENUM, 'TRUCK', l.EINGANG, l.VEHICLENUM), to_char(l.docid), C.TM, case when C.WARE in ('BW51048',
'N2127','N2144','N2211L-L','N2211L-R','N2211M-L','N2211M-R','N2212L-L','N2212L-R','N2213L-L','N2213L-R','N2221M','N2222L-L','N2222L-R','N2222M-L','N2222M-R','N2223L-R','N2223M-R',
'N2224L-L','N2224L-R','N2224M-L','N2224M-R','N2224XL-L','N2224XL-R','N2225L-L','N2225L-R','N2225M-L','N2225M-R','N2225XL-L','N2225XL-R','N2226L-L','N2226L-R','N2229M','N2310-L','N2310-R','N2402','VD58426NP',
'VD58876','VD58890','VD68797') then '���' else T.TYPENAME end, l.ddp   
from RC.LOGISTICS_SPECIFICATION_TABLE l
    inner join RC.CATALOG_WARE c on l.MONIKER = C.WARE
    left join RC.TYPE_PREPARATION_LM7 t on C.TYPE_PREPARATION = T.TYPEID    
where L.DESTPLACES = '������, ��������' and L.TOSTOR in ('������, ��������', '�������_������')
    and L.FROMSTOR in ('���� - ��� "��������� ������������� �������������� ������"','ITL-Cargo GmbH','B2B DE HUB (Hamburg)','�����','SDV Geis GmbH','(EEM) LOGO EE CY TLL (Tallinn)','(EEM) NKM CFS Kotka')
    and TITLEDOCSTATE = '� ����'
    AND l.ddp >= trunc(sysdate - 75) 
union all
 --��������� �� ����
select distinct DECODE(TITLEDOCSTATE, '� ����', '���� � ����', '� �����'), l.TOSTOR, DECODE(l.VEHICLENUM, 'TRUCK', l.EINGANG, l.VEHICLENUM), to_char(l.docid), C.TM, case when C.WARE in ('BW51048',
'N2127','N2144','N2211L-L','N2211L-R','N2211M-L','N2211M-R','N2212L-L','N2212L-R','N2213L-L','N2213L-R','N2221M','N2222L-L','N2222L-R','N2222M-L','N2222M-R','N2223L-R','N2223M-R',
'N2224L-L','N2224L-R','N2224M-L','N2224M-R','N2224XL-L','N2224XL-R','N2225L-L','N2225L-R','N2225M-L','N2225M-R','N2225XL-L','N2225XL-R','N2226L-L','N2226L-R','N2229M','N2310-L','N2310-R','N2402','VD58426NP',
'VD58876','VD58890','VD68797') then '���' else T.TYPENAME end, l.ddp   
from RC.LOGISTICS_SPECIFICATION_TABLE l
    inner join RC.CATALOG_WARE c on l.MONIKER = C.WARE
    left join RC.TYPE_PREPARATION_LM7 t on C.TYPE_PREPARATION = T.TYPEID    
where L.DESTPLACES = '������, ��������' 
    and L.TOSTOR in ('���� - ��� "��������� ������������� �������������� ������"','ITL-Cargo GmbH','B2B DE HUB (Hamburg)','�����','SDV Geis GmbH','(EEM) LOGO EE CY TLL (Tallinn)','(EEM) NKM CFS Kotka')
    AND l.ddp >= trunc(sysdate - 75);